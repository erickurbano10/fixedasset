package org.jo.logistics.fixedasset.view.ui;


import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

/**
 * Se crea una tarea o hilo para gestionar los mensajes que se mostraran en la
 * barra de mensajes
 * 
 * @author Fabian Andres Caciedo Cuellar
 *
 */
public class MessageBarTask extends Task<Void> {

	private AnchorPane acpMessage;
	private Label lblMessage;
	private String message;
	private MessageLevel msgLevel;
	private Integer milisegundos;

	public MessageBarTask(AnchorPane acpMessage, Label lblMessage, String message, MessageLevel msgLevel,
			Integer milisegundos) {
		super();
		this.acpMessage = acpMessage;
		this.lblMessage = lblMessage;
		this.message = message;
		this.msgLevel = msgLevel;
		this.milisegundos = milisegundos;
	}

	@Override
	protected Void call() throws Exception {
		Thread.sleep(milisegundos);
		return null;
	}

	@Override
	protected void running() {
		super.running();
		lblMessage.setTextFill(Color.BLACK);
		lblMessage.setStyle("-fx-font-weight: bold;");
		lblMessage.setText(message);
		if (msgLevel == MessageLevel.SUCCESS) {
			acpMessage.setStyle("-fx-background-color: #2ECC71;");
		}
		if (msgLevel == MessageLevel.EXCEPTION) {
			acpMessage.setStyle("-fx-background-color: #E74C3C;");
		}
		if (msgLevel == MessageLevel.WARNING) {
			acpMessage.setStyle("-fx-background-color: #F1C40F;");
		}
	}

	@Override
	protected void succeeded() {
		super.succeeded();
		lblMessage.setText(null);
		acpMessage.setStyle(null);
	}

	@Override
	protected void cancelled() {
		super.cancelled();
		lblMessage.setText(null);
		acpMessage.setStyle(null);
	}

	@Override
	protected void failed() {
		super.failed();
		lblMessage.setText(null);
		acpMessage.setStyle(null);
	}

}