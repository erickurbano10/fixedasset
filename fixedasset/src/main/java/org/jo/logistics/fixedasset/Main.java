package org.jo.logistics.fixedasset;

import java.io.IOException;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
	private Stage primaryStage;
	private static HBox mainLayount;

	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Login");
		Stage stage = new Stage();
		stage.centerOnScreen();
		stage.setMaximized(true);
		showMainView();

	}

	public void showMainView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/fxml/MainView.fxml"));
		mainLayount = loader.load();
		Scene scene = new Scene(mainLayount);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setMaximized(true);
	}

	public static void main(String[] args) {
		launch(args);

	}
}
