package org.jo.logistics.fixedasset.model;

import java.util.Date;
public class MaintenanceInformation {

	private Integer idMaintenance;
	private Integer idAsset;
	private Integer codeCompany;
	private Integer idEmployee;
	private Integer board;
	private String serialNumber;
	private String model;
	private String description;
	private String status;
    private Date  date ;
 	private String userCreated;
	private String userModificated;
	private Date dateCreated;
	private Date dateModificated;
	
	private String commentary;

	public MaintenanceInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MaintenanceInformation(Integer idMaintenance, Integer idAsset, Integer codeCompany, Integer idEmployee,
			String description, Date date, String userCreated, String userModificated,
			Date dateCreated, Date dateModificated, String commentary) {
		super();
		this.idMaintenance = idMaintenance;
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.idEmployee = idEmployee;
		this.description = description;
		this.date = date;
		this.userCreated = userCreated;
		this.userModificated = userModificated;
		this.dateCreated = dateCreated;
		this.dateModificated = dateModificated;
		this.commentary = commentary;
	}

	public Integer getIdMaintenance() {
		return idMaintenance;
	}

	public void setIdMaintenance(Integer idMaintenance) {
		this.idMaintenance = idMaintenance;
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Integer getBoard() {
		return board;
	}

	public void setBoard(Integer board) {
		this.board = board;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}



	




}
