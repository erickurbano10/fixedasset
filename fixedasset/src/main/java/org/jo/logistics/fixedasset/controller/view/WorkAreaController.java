package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.WorkArea;
import org.jo.logistics.fixedasset.model.WorkAreaInformation;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class WorkAreaController implements Initializable {
	
	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private MenuItem mnuWorkArea;

	@FXML
	private MenuItem mnuEmployee;
	

	@FXML
	private TextField txtName;

	@FXML
	private TextField txtDescription;

	
	@FXML
	private TextField txtCode;

	@FXML
	private Label lblDescription;

	@FXML
	private Label lblCode;

	@FXML
	private Button btnModify;

	@FXML
	private Button btnCancel;

	@FXML
	private Label lblName;

	@FXML
	private Button btnNew;

	@FXML
	private Button btnDelete;

	@FXML
	private Label lblTitle;

	@FXML
	private TableView<WorkAreaInformation> tblWorkArea;
	
	@FXML
	private TableColumn<WorkAreaInformation, String> tbcCode;

	@FXML
	private TableColumn<WorkAreaInformation, String> tbcDescription;
	
	@FXML
	private TableColumn<WorkAreaInformation, String> tbcName;


	@FXML
	private AnchorPane ancMessage;

	@FXML
	private Label lblMessage;

	private MessageBarTask messageBarTask;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		fillTblWorkArea();
		initBtnNew();
		initBtnDelete();
		initBtnModify();
		initBtnCancel();
		initTxtCode();
		initTxtName();
		initTxtDescription();
		btnCancel.setDisable(true);
		tblWorkArea.setDisable(true);
		txtCode.setEditable(false);
		txtName.setEditable(false);
		txtDescription.setEditable(false);
	}

	public void fillTblWorkArea() {

		tblWorkArea.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> observableValue, Object oldValue, Object newValue) {
				if (tblWorkArea.getSelectionModel().getSelectedItem() != null) {
					txtCode.setText(tblWorkArea.getSelectionModel().getSelectedItem().getCode());
					txtName.setText(tblWorkArea.getSelectionModel().getSelectedItem().getName());
					txtDescription.setText(tblWorkArea.getSelectionModel().getSelectedItem().getDescription());
				}
			}

		});

		tblWorkArea.getItems().clear();
		tbcCode.setCellValueFactory(new PropertyValueFactory<WorkAreaInformation, String>("code"));
		tbcName.setCellValueFactory(new PropertyValueFactory<WorkAreaInformation, String>("name"));
		tbcDescription.setCellValueFactory(new PropertyValueFactory<WorkAreaInformation, String>("description"));

		ObservableList<WorkAreaInformation> tblItems = new SimpleListProperty<WorkAreaInformation>(
				FXCollections.<WorkAreaInformation>observableArrayList());

		try {
			List<WorkAreaInformation> info = FixedAssetDB.getWorkArea(TemporaryVariables.codeCompanyLogin);

			for (WorkAreaInformation workAreaInformation : info) {
				tblItems.add(workAreaInformation);
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tblWorkArea.setItems(tblItems);
		tblWorkArea.refresh();
	}

	public void initBtnNew() {
		btnNew.setOnAction(Event -> {
			txtCode.requestFocus();

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {
				boolean entra = false;
			

				@Override
				protected Void call() throws Exception {

					btnCancel.setDisable(false);
					btnModify.setDisable(true);
					btnDelete.setDisable(true);
					txtCode.setEditable(true);
					txtName.setEditable(true);
					txtDescription.setEditable(true);

					if (!txtCode.getText().equals("") && !txtDescription.getText().equals("")
							&& !txtName.getText().equals("") && txtCode.getText() != null
							&& txtDescription.getText() != null && txtName.getText() != null) {
						Date date = new Date();
						WorkArea work = new WorkArea(txtCode.getText(), TemporaryVariables.codeCompanyLogin,
								txtName.getText(), txtDescription.getText(), TemporaryVariables.userNameLogin, date,
								TemporaryVariables.userNameLogin, date);
						try {
							if (FixedAssetDB.insertWorkArea(work)) {
								clearTxt();
								fillTblWorkArea();
								txtCode.setEditable(false);
								txtName.setEditable(false);
								txtDescription.setEditable(false);
								btnCancel.setDisable(true);
								btnModify.setDisable(false);
								btnDelete.setDisable(false);
								entra = true;
							} else {
								throw new Exception();
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					if (entra) {
						clearMessageBar();
						printMessageBar("Area de trabajo creada correctamente", MessageLevel.SUCCESS);
					}
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al crear el area de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});

	}

	public void initBtnDelete() {
		btnDelete.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {
				boolean entra = false;

				@Override
				protected Void call() throws Exception {
					tblWorkArea.setDisable(false);
					btnCancel.setDisable(false);
					btnModify.setDisable(true);
					btnNew.setDisable(true);
					if (tblWorkArea.getSelectionModel().getSelectedItem() != null) {
						try {
							if (FixedAssetDB.deleteWorkArea(tblWorkArea.getSelectionModel().getSelectedItem())) {
								fillTblWorkArea();
								tblWorkArea.setDisable(true);
								btnModify.setDisable(false);
								btnNew.setDisable(false);
								clearTxt();
								entra = true;
							} else {
								throw new Exception();
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					if (entra) {
						clearMessageBar();
						printMessageBar("Area de trabajo eliminada correctamente", MessageLevel.SUCCESS);
					}
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar el area de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	public void initBtnModify() {
		btnModify.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {
				boolean entra = false;

				@Override
				protected Void call() throws Exception {
					tblWorkArea.setDisable(false);
					btnCancel.setDisable(false);
					btnNew.setDisable(true);
					btnDelete.setDisable(true);
					txtCode.setEditable(true);
					txtName.setEditable(true);
					txtDescription.setEditable(true);
					Date date = new Date();

					if (!txtCode.getText().equals("") && !txtDescription.getText().equals("")
							&& !txtName.getText().equals("") && txtCode.getText() != null
							&& txtDescription.getText() != null && txtName.getText() != null) {

						WorkArea work = new WorkArea(txtCode.getText(), TemporaryVariables.codeCompanyLogin,
								txtName.getText(), txtDescription.getText(), TemporaryVariables.userNameLogin, date,
								TemporaryVariables.userNameLogin, date);

						try {
							if (FixedAssetDB.updateWorkArea(work)) {
								clearTxt();
								fillTblWorkArea();
								tblWorkArea.setDisable(true);
								btnCancel.setDisable(true);
								btnNew.setDisable(false);
								btnDelete.setDisable(false);
								txtCode.setEditable(false);
								txtName.setEditable(false);
								txtDescription.setEditable(false);
								entra = true;
							} else {
								throw new Exception();
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					if (entra) {
						clearMessageBar();
						printMessageBar("Area de trabajo modificada correctamente", MessageLevel.SUCCESS);
					}
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al modificar el area de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	public void initBtnCancel() {
		btnCancel.setOnAction(Event -> {
			clearTxt();
			btnDelete.setDisable(false);
			btnModify.setDisable(false);
			btnNew.setDisable(false);
			btnCancel.setDisable(true);
			tblWorkArea.setDisable(true);
			tblWorkArea.getSelectionModel().clearSelection();
			txtCode.setEditable(false);
			txtDescription.setEditable(false);
			txtName.setEditable(false);
		});
	}

	public void initTxtCode() {
		txtCode.setTextFormatter(new TextFormatter<>((change) -> {
			change.setText(change.getText().toUpperCase());
			return change;
		}));

	}

	public void initTxtName() {
		txtName.setTextFormatter(new TextFormatter<>((change) -> {
			change.setText(change.getText().toUpperCase());
			return change;
		}));

	}

	public void initTxtDescription() {
		txtDescription.setTextFormatter(new TextFormatter<>((change) -> {
			change.setText(change.getText().toUpperCase());
			return change;
		}));

	}

	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessage, lblMessage, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}

	public void clearTxt() {
		txtCode.setText("");
		txtName.setText("");
		txtDescription.setText("");
	}
	
	
	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
