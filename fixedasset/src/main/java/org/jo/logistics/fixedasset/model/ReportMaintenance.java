package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class ReportMaintenance {

	private Integer idMaintenance;
	private Integer idEmployee;
	private String nameEmployee;
	private String nameAsset;
	private String lastEmployee;
	private String description;
	private String descriptionm;
	private Date date;
	private String monthDate;
	private String yearDate;
	private String nameCompany;
	private String board;
	private String dayDate;
	private String nameArea;

	public ReportMaintenance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getNameEmployee() {
		return nameEmployee;
	}

	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}

	public String getNameAsset() {
		return nameAsset;
	}

	public void setNameAsset(String nameAsset) {
		this.nameAsset = nameAsset;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionm() {
		return descriptionm;
	}

	public void setDescriptionm(String descriptionm) {
		this.descriptionm = descriptionm;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getIdMaintenance() {
		return idMaintenance;
	}

	public void setIdMaintenance(Integer idMaintenance) {
		this.idMaintenance = idMaintenance;
	}

	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}

	public String getLastEmployee() {
		return lastEmployee;
	}

	public void setLastEmployee(String lastEmployee) {
		this.lastEmployee = lastEmployee;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}
 
	

	public String getDayDate() {
		return dayDate;
	}

	public void setDayDate(String dayDate) {
		this.dayDate = dayDate;
	}

	public String getMonthDate() {
		return monthDate;
	}

	public void setMonthDate(String monthDate) {
		this.monthDate = monthDate;
	}

	public String getYearDate() {
		return yearDate;
	}

	public void setYearDate(String yearDate) {
		this.yearDate = yearDate;
	}

	public String getNameArea() {
		return nameArea;
	}

	public void setNameArea(String nameArea) {
		this.nameArea = nameArea;
	}

	
	
	
	

}
