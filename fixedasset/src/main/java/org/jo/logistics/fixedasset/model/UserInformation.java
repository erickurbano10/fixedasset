package org.jo.logistics.fixedasset.model;

public class UserInformation {

	private String user;
	private String name;
	private String lastName;
	private String email;
	private String password;
	private int idRol;
	private int idUser;
	private String active;
	private int company;

	public UserInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserInformation(String user, String name, String lastName, String email, String password, int idRol,
			int idUser, String active, int company) {
		super();
		this.user = user;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.idRol = idRol;
		this.idUser = idUser;
		this.active = active;
		this.company = company;
	}

	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

}
