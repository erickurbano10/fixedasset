package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.ComboElement;
import org.jo.logistics.fixedasset.model.DeliveryDetInformation;
import org.jo.logistics.fixedasset.model.DeliveryEnc;
import org.jo.logistics.fixedasset.model.DeliveryEncInformation;
import org.jo.logistics.fixedasset.model.FixedAssetInformation;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class OptionDeliveryController implements Initializable {

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuUserRegister;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private SearchComponent cmbemployee;

	@FXML
	private Label lblemployee;

	@FXML
	private AnchorPane tblFixed;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcFixed;

	@FXML
	private TitledPane tblCertificate;

	@FXML
	private TableView<DeliveryDetInformation> tblDeliveryDet;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcBoard;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcModel;

	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnNew;

//	@FXML
//	private AnchorPane ancMessage;
//
//	@FXML
//	private Label lblMessage;

//	private MessageBarTask messageBarTask;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillCmbEmployee();
		initBtnGenerate();
	}

	public void fillCmbEmployee() {

		ObservableList<ItemInformation> listEmployee = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAssetEmployeeDown();

			for (ItemInformation itemInformation : list) {
				listEmployee.addAll(itemInformation);

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmbemployee.initCmbSearch(listEmployee);
		cmbemployee.initBtnClear();
		cmbemployee.fillCmb(listEmployee);

	}

	public void initBtnGenerate() {
		btnGenerate.setOnAction(event -> {
			tbcBoard.setCellValueFactory(new PropertyValueFactory<>("assetBoard"));
			tbcModel.setCellValueFactory(new PropertyValueFactory<>("assetModel"));
			tbcFixed.setCellValueFactory(new PropertyValueFactory<>("assetName"));
			ObservableList<DeliveryDetInformation> listItems = FXCollections.observableArrayList();
			try {
				List<DeliveryDetInformation> list = FixedAssetDB
						.searchEmployee(Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()),TemporaryVariables.codeCompanyLogin);

				for (DeliveryDetInformation delivery : list) {
					listItems.add(delivery);
				}

				tblDeliveryDet.setItems(listItems);
				tblDeliveryDet.refresh();

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		});

	}
	
	
	public void initBtnNew() {
		btnNew.setOnAction(event -> {
			
			DeliveryDetInformation delivery = new DeliveryDetInformation();
			
			try {
				Integer idDelivery = FixedAssetDB.idDeliveryEnc(Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()),TemporaryVariables.codeCompanyLogin);
			
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
			
			if (tblDeliveryDet.getSelectionModel().getSelectedItem() != null) {
				
			}
			
			
			
			
			
			
			
			
		});
	}

//	public void initBtnNew() {
//		btnNew.setOnAction(event -> {
//			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {
//
//				@Override
//				protected Void call() throws Exception {
//
//					return null;
//				}
//
//				@Override
//				protected void succeeded() {
//					super.succeeded();
//					this.getDialogStage().hide();
//					clearMessageBar();
//					printMessageBar("Acta de Entrega  creado correctamente", MessageLevel.SUCCESS);
//				}
//
//				@Override
//				protected void failed() {
//					super.failed();
//					this.getDialogStage().hide();
//					clearMessageBar();
//					printMessageBar("Error al eliminar Acta de Entrega", MessageLevel.EXCEPTION);
//				}
//
//			};
//
//			ExecutorService executorService = Executors.newFixedThreadPool(1);
//			executorService.execute(progressIndicatorSceneTask);
//			executorService.shutdown();
//
//		});
//	}
//
//	private void clearMessageBar() {
//		if (messageBarTask != null && messageBarTask.isRunning()) {
//			messageBarTask.cancel();
//		}
//	}
//
//	private void printMessageBar(String message, MessageLevel msgLevel) {
//		clearMessageBar();
//		messageBarTask = new MessageBarTask(ancMessage, lblMessage, message, msgLevel, 7000);
//		ExecutorService executorService = Executors.newFixedThreadPool(1);
//		executorService.execute(messageBarTask);
//		executorService.shutdown();
//	}

//	public void refresh() {
//
//		tbcBoard.setCellValueFactory(new PropertyValueFactory<>("assetBoard"));
//		tbcModel.setCellValueFactory(new PropertyValueFactory<>("assetModel"));
//		tbcFixed.setCellValueFactory(new PropertyValueFactory<>("assetName"));
//		ObservableList<DeliveryDetInformation> listItems = FXCollections.observableArrayList();
//		try {
//			List<DeliveryDetInformation> list = FixedAssetDB
//					.searchEmployee(Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()));
//
//			for (DeliveryDetInformation delivery : list) {
//				listItems.add(delivery);
//			}
//
//			tblDeliveryDet.setItems(listItems);
//			tblDeliveryDet.refresh();
//
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}

	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	// donde
	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
