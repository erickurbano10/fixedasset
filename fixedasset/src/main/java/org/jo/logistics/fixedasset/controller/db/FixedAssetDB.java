package org.jo.logistics.fixedasset.controller.db;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

import org.apache.commons.codec.binary.Base64;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.model.AssetDownInformation;
import org.jo.logistics.fixedasset.model.AssetRetunrInformation;
import org.jo.logistics.fixedasset.model.ComboElement;
import org.jo.logistics.fixedasset.model.Company;
import org.jo.logistics.fixedasset.model.DeliveryDet;
import org.jo.logistics.fixedasset.model.DeliveryDetInformation;
import org.jo.logistics.fixedasset.model.DeliveryEnc;
import org.jo.logistics.fixedasset.model.DeliveryEncInformation;
import org.jo.logistics.fixedasset.model.Employee;
import org.jo.logistics.fixedasset.model.EmployeeInformation;
import org.jo.logistics.fixedasset.model.FixedAsset;
import org.jo.logistics.fixedasset.model.FixedAssetInformation;
import org.jo.logistics.fixedasset.model.MaintenanceInformation;
import org.jo.logistics.fixedasset.model.ReportDeliveryAssetTwo;
import org.jo.logistics.fixedasset.model.ReportMaintenance;
import org.jo.logistics.fixedasset.model.UpgradeInformation;
import org.jo.logistics.fixedasset.model.User;
import org.jo.logistics.fixedasset.model.UserInformation;
import org.jo.logistics.fixedasset.model.WorkArea;
import org.jo.logistics.fixedasset.model.WorkAreaInformation;
import org.jo.logistics.fixedasset.model.WorkPosition;
import org.jo.logistics.fixedasset.model.WorkPositionInformation;
import org.jo.logistics.fixedasset.model.reportAsset;
import org.jo.logistics.fixedasset.model.reportDelivery;
import org.jo.logistics.fixedasset.model.reportRetunr;
import org.jo.logistics.fixedasset.model.reportUpgrade;

import com.sun.media.jfxmedia.events.NewFrameEvent;

public class FixedAssetDB {
	// ip 192.168.99.101
	// password reallyStrongPwd123
	public static String IP = "127.0.0.1";
	public static String PORT = "1433";
	public static String DATABASENAME = "FIXEDASSETS";
	public static String USER = "sa";
	public static String PASSWORD = "root";

	public static Connection connection() throws ClassNotFoundException, SQLException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection connection = null;
		if (DATABASENAME != null) {
			connection = DriverManager.getConnection("jdbc:sqlserver://" + IP + ":" + PORT + ";databaseName="
					+ DATABASENAME + ";user=" + USER + ";password=" + PASSWORD);
		} else {
			connection = DriverManager
					.getConnection("jdbc:sqlserver://" + IP + ":" + PORT + ";user=" + USER + ";password=" + PASSWORD);
		}
		return connection;
	}

	public static boolean login(User employee) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = connection();
		String sql = "SELECT \"idUser\",\"userName\",\"password\"  FROM \"Users\" WHERE \"userName\" = ? ";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, employee.getUser());
			rs = ps.executeQuery();
			if (rs.next()) {
				String pass = employee.getPass();
				pass = Encriptar(pass);

				if (pass.equals(rs.getString("password"))) {

					employee.setIduser(rs.getInt(1));
					employee.setUser(rs.getString(2));

					return true;
				} else {
					return false;
				}
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e);
			return false;
		}
		return false;
	}

	public static String validatePass(int idRol) throws ClassNotFoundException, SQLException {
		String pass = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = connection();
		String sql = "SELECT password FROM Users where idUser = ?";
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, idRol);
			rs = ps.executeQuery();

			if (rs.isBeforeFirst()) {
				rs.next();
				pass = rs.getString("password");
			}
		} catch (Exception e) {
		}
		rs.close();
		ps.close();
		con.close();
		return pass;
	}

	public static boolean register(User emp) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO \"Users\" (\"userName\",\"name\",\"lastName\",\"email\",\"password\",\"idRol\",\"active\",\"company\",\"userCreated\",\"dateCreated\",\"userModificated\",\"dateModificated\")  VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, emp.getUser());
			ps.setString(2, emp.getName());
			ps.setString(3, emp.getLastName());
			ps.setString(4, emp.getEmail());
			ps.setString(5, emp.getPass());
			ps.setInt(6, emp.getIdRol());
			ps.setString(7, emp.getStatus());
			ps.setInt(8, emp.getCompany());
			ps.setString(9, emp.getUserCreated());
			ps.setTimestamp(10, new Timestamp(emp.getDateCreated().getTime()));
			ps.setString(11, emp.getUserModificated());
			ps.setTimestamp(12, new Timestamp(emp.getDateModificated().getTime()));

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean insertEmployee(Employee emp) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO \"Employee\" (\"codeCompany\",\"name\",\"lastName\",\"codeWorkPosition\",\"codeWorkArea\",\"userCreated\",\"dateCreated\",\"userModified\",\"dateModified\") VALUES (?,?,?,?,?,?,?,?,?);";

		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, emp.getCodeCompany());
			ps.setString(2, emp.getName());
			ps.setString(3, emp.getLastName());
			ps.setString(4, emp.getCodeWorkPosition());
			ps.setString(5, emp.getCodeWorkArea());
			ps.setString(6, emp.getUserCreated());
			ps.setTimestamp(7, new Timestamp(emp.getDateCreated().getTime()));
			ps.setString(8, emp.getUserModified());
			ps.setTimestamp(9, new Timestamp(emp.getDateModified().getTime()));

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating employee failed, no rows affected.");

			} else {
				con.commit();
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static List<Company> codeCompany() throws ClassNotFoundException, SQLException {
		String query = "Select codeCompany,name FROM  Company ";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<Company> datos = new ArrayList<Company>();
		con = connection();
		pst = con.prepareStatement(query);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				Company item = new Company();
				item.setCodeCompany(rs.getInt(1));
				item.setDescription(rs.getString(2));

				datos.add(item);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static boolean modificated(User emp) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE Users SET userName = ?, name = ?,lastName = ?, email = ?, password = ? ,active = ?,idRol = ?, company = ?, userModificated = ? , dateModificated = ? WHERE idUser = ?";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, emp.getUser());
			ps.setString(2, emp.getName());
			ps.setString(3, emp.getLastName());
			ps.setString(4, emp.getEmail());
			ps.setString(5, emp.getPass());
			ps.setString(6, emp.getStatus());
			ps.setInt(7, emp.getIdRol());
			ps.setInt(8, emp.getCompany());
			ps.setString(9, emp.getUserModificated());
			ps.setTimestamp(10, new Timestamp(emp.getDateModificated().getTime()));
			ps.setInt(11, emp.getIduser());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Modity user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean delete(User emp) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete  from Users where idUser = ?";
		try {

			ps = con.prepareStatement(query);
			ps.setInt(1, emp.getIduser());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Delete user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static List<UserInformation> refresh() throws ClassNotFoundException, SQLException {
		String sql = "SELECT userName,name,lastName,email,password,idRol,idUser FROM Users ";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<UserInformation> datos = new ArrayList<UserInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				UserInformation us = new UserInformation();
				us.setUser(rs.getString(1));
				us.setName(rs.getString(2));
				us.setLastName(rs.getString(3));
				us.setEmail(rs.getString(4));
				us.setPassword(rs.getString(5));
				us.setIdRol(rs.getInt(6));
				us.setIdUser(rs.getInt(7));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<FixedAssetInformation> getFixeds(Integer codeCompany, Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT \"idAsset\",\"codeCompany\",\"board\",\"serialNumber\",\"model\",\"description\",\"status\",\"idEmployee\",\"userCreated\",\"dateCreated\",\"userModificated\",\"dateModificated\",\"frontPicture\",\"backPicture\" FROM \"FixedAsset\" WHERE \"codeCompany\" = ? and \"idEmployee\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<FixedAssetInformation> datos = new ArrayList<FixedAssetInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setInt(2, idEmployee);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				FixedAssetInformation us = new FixedAssetInformation();
				us.setIdAsset(rs.getInt("idAsset"));
				us.setCodeCompany(rs.getInt("codeCompany"));
				us.setBoard(rs.getString("board"));
				us.setSerial(rs.getString("serialNumber"));
				us.setModel(rs.getString("model"));
				us.setDescription(rs.getString("description"));
				if (rs.getString("status").equals("A")) {
					us.setStatus("ASIGNADO");
				} else {
					us.setStatus("LIBRE");
				}
				us.setIdEmployee(rs.getInt("idEmployee"));
				us.setImgBack(rs.getBytes("backPicture"));
				us.setImgFront(rs.getBytes("frontPicture"));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static String getNameRol(Integer idRol) throws ClassNotFoundException, SQLException {
		String name = "";

		Connection con;
		con = connection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = "Select name FROM Role Where idRol = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, idRol);
			rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				name = rs.getString("name");

			}
		} catch (Exception e) {

		}
		rs.close();
		ps.close();
		con.close();
		return name;
	}

	public static String getCompany(Integer idCompany) throws ClassNotFoundException, SQLException {
		String name = "";
		Connection con;
		con = connection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = "Select name FROM  Company Where codeCompany = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, idCompany);
			rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				name = rs.getString("name");
			}
		} catch (Exception e) {
		}
		rs.close();
		ps.close();
		con.close();
		return name;
	}

	public static Integer getCodeCompany(String Company) throws ClassNotFoundException, SQLException {
		Integer code = null;
		Connection con;
		con = connection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = "Select codeCompany FROM  Company Where name = ?";
			ps = con.prepareStatement(query);
			ps.setString(1, Company);
			rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				code = rs.getInt("codeCompany");
			}
		} catch (Exception e) {
		}
		rs.close();
		ps.close();
		con.close();
		return code;
	}

	public static String getNameStatus(Integer idUser) throws ClassNotFoundException, SQLException {
		String name = "";
		Connection con;
		con = connection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = "Select active FROM Users Where idUser = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, idUser);
			rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				name = rs.getString("active");

			}
		} catch (Exception e) {
		}

		if (name.equals("A")) {
			name = "Active";
		} else {
			name = "Inactive";
		}
		rs.close();
		ps.close();
		con.close();
		return name;
	}

	public static String getCodeWorkArea(String name, Integer codeCompany) throws ClassNotFoundException, SQLException {
		String code = "";
		Connection con;
		con = connection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = "Select \"code\" FROM \"WorkArea\" Where \"name\" = ? and codeCompany = ?";
			ps = con.prepareStatement(query);
			ps.setString(1, name);
			ps.setInt(2, codeCompany);
			rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				code = rs.getString("code");
			}
		} catch (Exception e) {
		}

		rs.close();
		ps.close();
		con.close();
		return code;
	}

	public static boolean showClick() throws ClassNotFoundException, SQLException {
		Connection con;
		con = connection();
		PreparedStatement ps;
		try {
			String query = "SELECT userName,name,lastName,email,password,idRol,idUser,active,company FROM Users ";
			ps = con.prepareStatement(query);

		} catch (Exception e) {
		}
		return false;
	}

	public static boolean insertWorkArea(WorkArea workarea) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO \"WorkArea\" (\"code\",\"codeCompany\",\"name\",\"description\",\"userCreated\",\"dateCreated\",\"userModified\",\"dateModified\")  VALUES (?,?,?,?,?,?,?,?)";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, workarea.getCode());
			ps.setInt(2, workarea.getCodeCompany());
			ps.setString(3, workarea.getName());
			ps.setString(4, workarea.getDescription());
			ps.setString(5, workarea.getUserCreated());
			ps.setTimestamp(6, new Timestamp(workarea.getDateCreated().getTime()));
			ps.setString(7, workarea.getUserModified());
			ps.setTimestamp(8, new Timestamp(workarea.getDateModified().getTime()));

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating workarea failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}

	}

	public static List<WorkAreaInformation> getWorkArea(Integer codeCompany)
			throws ClassNotFoundException, SQLException {
		String sql = "select \"code\",\"name\",\"description\",\"codeCompany\" from \"WorkArea\" where \"codeCompany\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<WorkAreaInformation> datos = new ArrayList<WorkAreaInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				WorkAreaInformation us = new WorkAreaInformation();
				us.setCode(rs.getString("code"));
				us.setName(rs.getString("name"));
				us.setDescription(rs.getString("description"));
				us.setCodeCompany(rs.getInt("codeCompany"));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {

		}
		return datos;
	}

	public static List<EmployeeInformation> getEmployee(Integer codeCompany, String codeWorkArea)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"idEmployee\", T0.\"codeCompany\", T0.\"name\", T0.\"lastName\", T0.\"codeWorkPosition\", T0.\"codeWorkArea\", T0.\"userCreated\", T0.\"dateCreated\", T0.\"userModificated\", T0.\"dateModificated\",T1.\"name\" as \"nameWorkArea\", T2.\"name\" as \"nameWorkPosition\" FROM \"Employee\" T0 INNER JOIN \"WorkArea\" T1 ON T0.\"codeWorkArea\" = T1.\"code\" and T0.\"codeCompany\" = T1.\"codeCompany\" INNER JOIN \"WorkPosition\" T2 ON T0.\"codeWorkPosition\" = T2.\"code\" and T0.\"codeCompany\" = T2.\"codeCompany\" AND T1.\"code\" = T2.\"codeWorkArea\" WHERE T0.\"codeCompany\" = ? and T0.\"codeWorkArea\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<EmployeeInformation> datos = new ArrayList<EmployeeInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setString(2, codeWorkArea);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				EmployeeInformation us = new EmployeeInformation();
				us.setCodeCompany(rs.getInt("codeCompany"));
				us.setCodeWorkArea(rs.getString("codeWorkArea"));
				us.setCodeWorkPosition(rs.getString("codeWorkPosition"));
				us.setDateCreated(rs.getDate("dateCreated"));
				us.setDateModificated(rs.getDate("dateModified"));
				us.setIdEmployee(rs.getInt("idEmployee"));
				us.setLastName(rs.getString("lastName"));
				us.setName(rs.getString("name"));
				us.setNameWorkArea(rs.getString("nameWorkArea"));
				us.setNameWorkPosition(rs.getString("nameWorkPosition"));
				us.setUserCreated(rs.getString("userCreated"));
				us.setUserModificated(rs.getString("userModificated"));
				datos.add(us);

			}
		} catch (SQLException e) {
		}
		return datos;
	}

	public static List<ComboElement> getEmployeeCmb(Integer codeCompany, String codeWorkArea)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"idEmployee\", T0.\"name\" FROM \"Employee\" T0 INNER JOIN \"WorkArea\" T1 ON T0.\"codeWorkArea\" = T1.\"code\" and T0.\"codeCompany\" = T1.\"codeCompany\" INNER JOIN \"WorkPosition\" T2 ON T0.\"codeWorkPosition\" = T2.\"code\" and T0.\"codeCompany\" = T2.\"codeCompany\" AND T1.\"code\" = T2.\"codeWorkArea\" WHERE T0.\"codeCompany\" = ? and T0.\"codeWorkArea\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ComboElement> datos = new ArrayList<ComboElement>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setString(2, codeWorkArea);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ComboElement us = new ComboElement();
				us.setCode(rs.getString("idEmployee"));
				us.setDescription(rs.getString("name"));
				datos.add(us);
			}
		} catch (SQLException e) {
		}
		return datos;
	}

	public static List<DeliveryEncInformation> getDeliveryEncForEmployee(Integer codeCompany, Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"idDelivery\",T0.\"codeCompany\",T0.\"comments\",T0.\"userCreated\",T0.\"dateCreated\",T0.\"userModificated\",T0.\"dateModificated\",T0.\"deliveryDate\",T0.\"idEmployee\",T0.\"idHeadArea\",T1.\"name\"+' '+T1.lastName AS \"name\" FROM \"DeliveryCertificate_Enc\" T0 INNER JOIN \"Employee\" T1 ON T0.\"codeCompany\" = T1.\"codeCompany\" AND T0.\"idHeadArea\" = T1.\"idEmployee\" WHERE T0.\"codeCompany\" = ? and T0.\"idEmployee\"  = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<DeliveryEncInformation> datos = new ArrayList<DeliveryEncInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setInt(2, idEmployee);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				DeliveryEncInformation us = new DeliveryEncInformation();
				us.setIdDelivery(rs.getInt("idDelivery"));
				us.setCodeCompany(codeCompany);
				us.setComments(rs.getString("comments"));
				us.setUserCreated(rs.getString("userCreated"));
				us.setDateCreated(rs.getDate("dateCreated"));
				us.setUserModificated(rs.getString("userModificated"));
				us.setDateModificated(rs.getDate("dateModificated"));
				us.setDeliveryDate(rs.getDate("deliveryDate").toLocalDate());
				us.setIdEmployee(rs.getInt("idEmployee"));
				us.setIdHeadArea(rs.getInt("idHeadArea"));
				us.setHeadArea(rs.getString("name"));
				datos.add(us);
			}
		} catch (SQLException e) {
		}
		return datos;
	}

	public static List<DeliveryDetInformation> getDeliveryDetForIdEnc(Integer codeCompany)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT idAsset,description,board,model,codeCompany FROM FixedAsset WHERE  codeCompany = ? AND status = 'L'";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<DeliveryDetInformation> datos = new ArrayList<DeliveryDetInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				DeliveryDetInformation us = new DeliveryDetInformation();
				us.setIdAsset(rs.getInt("idAsset"));
				us.setAssetName(rs.getString("description"));
				us.setAssetBoard(rs.getString("board"));
				us.setAssetModel(rs.getString("model"));
				us.setCodeCompany(rs.getInt("codeCompany"));
				us.setSelect(true);

				datos.add(us);
			}
		} catch (SQLException e) {
		}
		return datos;
	}

	public static boolean updateWorkArea(WorkArea work) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE \"WorkArea\" SET code = ?, name = ?, description = ?, userModified = ?, dateModified = ? WHERE code = ? and codeCompany = ?";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, work.getCode());
			ps.setString(2, work.getName());
			ps.setString(3, work.getDescription());
			ps.setString(4, work.getUserModified());
			ps.setTimestamp(5, new Timestamp(work.getDateModified().getTime()));
			ps.setString(6, work.getCode());
			ps.setInt(7, work.getCodeCompany());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Modify workarea failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean updateFixedAsset(FixedAsset fixed) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE \"FixedAsset\" SET \"board\" = ?, \"serialNumber\" = ?, \"model\" = ?, \"description\" =?, \"userModificated\" = ? ,\"dateModificated\" = ?,\"frontPicture\" = ?, \"backPicture\" = ? WHERE \"codeCompany\" = ? and \"idAsset\" = ?;";

		try {

			ps = con.prepareStatement(sql);
			ps.setString(1, fixed.getBoard());
			ps.setString(2, fixed.getSerial());
			ps.setString(3, fixed.getModel());
			ps.setString(4, fixed.getDescription());
			ps.setString(5, fixed.getUserModified());
			ps.setTimestamp(6, new Timestamp(fixed.getDateModified().getTime()));
			ps.setBytes(7, fixed.getFrontPicture());
			ps.setBytes(8, fixed.getBackPicture());
			ps.setInt(9, fixed.getCodeCompany());
			ps.setInt(10, fixed.getIdAsset());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Modify Asset failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean deleteWorkArea(WorkAreaInformation work) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete from \"WorkArea\" where \"code\" = ? and \"codeCompany\"= ?";
		try {
			ps = con.prepareStatement(query);
			ps.setString(1, work.getCode());
			ps.setInt(2, work.getCodeCompany());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Delete workArea failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean insertWorkPosition(WorkPosition workposition) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO \"WorkPosition\" (\"code\",\"codeCompany\",\"codeWorkArea\",\"name\",\"description\",\"userCreated\",\"dateCreated\",\"userModified\",\"dateModified\",\"headArea\")  VALUES (?,?,?,?,?,?,?,?,?,?)";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, workposition.getCode());
			ps.setInt(2, workposition.getCodeCompany());
			ps.setString(3, workposition.getCodeWorkArea());
			ps.setString(4, workposition.getName());
			ps.setString(5, workposition.getDescription());
			ps.setString(6, workposition.getUserCreated());
			ps.setTimestamp(7, new Timestamp(workposition.getDateCreated().getTime()));
			ps.setString(8, workposition.getUserModified());
			ps.setTimestamp(9, new Timestamp(workposition.getDateModified().getTime()));
			ps.setBoolean(10, workposition.getHeadArea());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating workposition failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}

	}

	public static boolean insertFixedAsset(FixedAsset fixed) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO \"FixedAsset\"(\"codeCompany\",\"board\",\"serialNumber\",\"model\",\"description\",\"status\",\"idEmployee\",\"userCreated\",\"dateCreated\",\"userModificated\",\"dateModificated\",\"frontPicture\",\"backPicture\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";

		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, fixed.getCodeCompany());
			ps.setString(2, fixed.getBoard());
			ps.setString(3, fixed.getSerial());
			ps.setString(4, fixed.getModel());
			ps.setString(5, fixed.getDescription());
			if (fixed.getStatus().equals("ASIGNADO")) {
				ps.setString(6, "A");
			} else {
				ps.setString(6, "L");
			}
			ps.setInt(7, fixed.getIdEmployee());
			ps.setString(8, fixed.getUserCreated());
			ps.setTimestamp(9, new Timestamp(fixed.getDateCreated().getTime()));
			ps.setString(10, fixed.getUserModified());
			ps.setTimestamp(11, new Timestamp(fixed.getDateModified().getTime()));
			ps.setBytes(12, fixed.getFrontPicture());
			ps.setBytes(13, fixed.getBackPicture());
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating workposition failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}

	}

	public static boolean insertDeliveryEnc(DeliveryEnc deliveryenc) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO DeliveryCertificate_Enc(\"codeCompany\",\"comments\",\"userCreated\",\"dateCreated\",\"userModificated\",\"dateModificated\",\"deliveryDate\",\"idEmployee\",\"idHeadArea\",\"status\") values (?,?,?,?,?,?,?,?,?,'A');";

		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, deliveryenc.getCodeCompany());
			ps.setString(2, deliveryenc.getComments());
			ps.setString(3, deliveryenc.getUserCreated());
			ps.setTimestamp(4, new Timestamp(deliveryenc.getDateCreated().getTime()));
			ps.setString(5, deliveryenc.getUserModificated());
			ps.setTimestamp(6, new Timestamp(deliveryenc.getDateModificated().getTime()));
			ps.setTimestamp(7, new Timestamp(deliveryenc.getDeliveryDate().getTime()));
			ps.setInt(8, deliveryenc.getIdEmployee());
			ps.setInt(9, deliveryenc.getIdHeadArea());
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating deliveryEnc failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}

	}

//	public static List<DeliveryDetInformation> getDeliveryDetForIdEnc(Integer codeCompany)
//			throws ClassNotFoundException, SQLException {
//		String sql = "SELECT T0.idDelivery_det,T0.idAsset,T1.description,T1.board,T1.model,T0.codeCompany,T0.idDelivery,T0.cant FROM \"DeliveryCertificate_Det\" T0 INNER JOIN \"FixedAsset\"T1 ON T0.\"codeCompany\" = T1.\"codeCompany\" AND T0.\"idAsset\" = T1.\"idAsset\" WHERE T0.codeCompany = ?   AND T1.status = 'L'  ";
//		Connection con;
//		PreparedStatement pst;
//		ResultSet rs = null;
//		List<DeliveryDetInformation> datos = new ArrayList<DeliveryDetInformation>();
//		con = connection();
//		pst = con.prepareStatement(sql);
//		pst.setInt(1, codeCompany);
////		pst.setInt(2, idEnc);
//		rs = pst.executeQuery();
//		try {
//
//			while (rs.next()) {
//				DeliveryDetInformation us = new DeliveryDetInformation();
//				us.setIdDeliveryDet(rs.getInt("idDelivery_det"));
//				us.setIdAsset(rs.getInt("idAsset"));
//				us.setAssetName(rs.getString("description"));
//				us.setAssetBoard(rs.getString("board"));
//				us.setAssetModel(rs.getString("model"));
//				us.setCodeCompany(rs.getInt("codeCompany"));
//				us.setIdDelivery(rs.getInt("idDelivery"));
//				us.setCant(rs.getInt("cant"));
//				us.setSelect(true);
////				us.setUserCreated(rs.getString("userCreated"));
////				us.setDateCreated(rs.getDate("dateCreated"));
////				us.setUserModificated(rs.getString("userModificated"));
////				us.setDateModificated(rs.getDate("dateModificated"));
//				datos.add(us);
//			}
//		} catch (SQLException e) {
//		}
//		return datos;
//	}

	public static Integer idDeliveryEnc(Integer idEmployee, Integer codeCompany)
			throws ClassNotFoundException, SQLException {
		Integer idDelivery = null;

		String sql = " SELECT  idDelivery from DeliveryCertificate_Enc WHERE idEmployee = ? and codeCompany = ? ";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, idEmployee);
		pst.setInt(2, codeCompany);

		rs = pst.executeQuery();
		try {
			while (rs.next()) {
				idDelivery = rs.getInt("idDelivery");
			}
		} catch (SQLException e) {
		}
		return idDelivery;
	}

	public static boolean insertDeliveryDet(DeliveryDetInformation deliverydet)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "INSERT INTO DeliveryCertificate_Det (idAsset,codeCompany,idDelivery,userCreated,dateCreated,cant,status) values (?,?,?,?,?,?,'A')";
        
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, deliverydet.getIdAsset());
			ps.setInt(2, deliverydet.getCodeCompany());
			ps.setInt(3, deliverydet.getIdDelivery());
			ps.setString(4, deliverydet.getUserCreated());
			ps.setTimestamp(5, new Timestamp(deliverydet.getDateCreated().getTime()));
			ps.setString(6, deliverydet.getCant());
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Creating deliveryEnc failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			e.getMessage();
			ps.close();
			con.close();
			return false;
		}

	}


	public static boolean UpdateStatus(Integer idAsset , Integer codeCompany,Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE FixedAsset SET idEmployee = ?, status = 'A'   WHERE idAsset = ? and codeCompany = ?";

		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, idEmployee);
			ps.setInt(2, idAsset);
			ps.setInt(3, codeCompany);
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Modity user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			e.getMessage();
			return false;
		}

	}
	
	
	public static List<DeliveryDetInformation> refreshDelivery() throws ClassNotFoundException, SQLException {
		String sql = "SELECT board,model,description FROM FixedAsset WHERE   status = 'L'";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<DeliveryDetInformation> datos = new ArrayList<DeliveryDetInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				DeliveryDetInformation us = new DeliveryDetInformation();
				us.setAssetBoard(rs.getString(1));
				us.setAssetModel(rs.getString(2));
				us.setAssetName(rs.getString(3));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}
	
	
	
	
	
	
	public static boolean updateWorkPosition(WorkPosition work) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE \"WorkPosition\" SET code = ?, codeWorkArea = ?, name = ?, description = ?, userModified = ?, dateModified = ?, headArea = ? WHERE code = ? and codeCompany = ? and codeWorkArea = ?";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, work.getCode());
			ps.setString(2, work.getCodeWorkArea());
			ps.setString(3, work.getName());
			ps.setString(4, work.getDescription());
			ps.setString(5, work.getUserModified());
			ps.setTimestamp(6, new Timestamp(work.getDateModified().getTime()));
			ps.setBoolean(7, work.getHeadArea());
			ps.setString(8, work.getCode());
			ps.setInt(9, work.getCodeCompany());
			ps.setString(10, work.getCodeWorkArea());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Modify workposition failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean updateEmployee(Employee emp) throws ClassNotFoundException, SQLException {

		PreparedStatement ps = null;
		Connection con = connection();
		String sql = "UPDATE \"Employee\" SET \"name\" = ?, \"lastName\" = ?, \"codeWorkPosition\" = ?, \"userModified\" = ?, \"dateModified\" = ? WHERE \"idEmployee\" = ? and \"codeCompany\"=?;";

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, emp.getName());
			ps.setString(2, emp.getLastName());
			ps.setString(3, emp.getCodeWorkPosition());
			ps.setString(4, emp.getUserModified());
			ps.setTimestamp(5, new Timestamp(emp.getDateModified().getTime()));
			ps.setInt(6, emp.getIdEmployee());
			ps.setInt(7, emp.getCodeCompany());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Modify employee failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static List<WorkPositionInformation> getWorkPositions(Integer codeCompany, String code)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"code\",T0.\"name\",T0.\"description\",T1.\"name\" as \"workarea\", T0.\"codeCompany\", T0.\"codeWorkArea\", T0.\"headArea\" FROM \"WorkPosition\" T0 INNER JOIN \"WorkArea\" T1 ON T0.\"codeWorkArea\" = T1.\"code\" and T0.\"codeCompany\" = T1.\"codeCompany\" where T0.\"codeCompany\" = ? and T0.\"codeWorkArea\"= ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<WorkPositionInformation> datos = new ArrayList<WorkPositionInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setString(2, code);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				WorkPositionInformation us = new WorkPositionInformation();
				us.setCode(rs.getString("code"));
				us.setName(rs.getString("name"));
				us.setDescription(rs.getString("description"));
				us.setNameWorkArea(rs.getString("workarea"));
				us.setCodeCompany(rs.getInt("codeCompany"));
				us.setCodeWorkArea(rs.getString("codeWorkArea"));
				us.setHeadArea(rs.getBoolean("headArea"));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {

		}
		return datos;
	}

	public static ItemInformation getHeadArea(Integer codeCompany, String codeWorkArea)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"idEmployee\",T0.\"name\"+' '+T0.\"lastName\" AS \"name\" FROM \"Employee\" T0 WHERE T0.\"codeCompany\" = ? AND T0.\"codeWorkArea\" = ? AND T0.\"codeWorkPosition\" IN (SELECT T1.\"code\" FROM \"WorkPosition\" T1 WHERE T1.\"codeCompany\" = T0.\"codeCompany\" AND T1.\"codeWorkArea\" = T0.\"codeWorkArea\" AND T1.\"headArea\" = 1);";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		con = connection();
		ItemInformation item = new ItemInformation();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		pst.setString(2, codeWorkArea);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				item.setItemCode(rs.getString("idEmployee"));
				item.setDscription(rs.getString("name"));
			}
		} catch (SQLException e) {

		}
		return item;
	}

	public static List<ItemInformation> getWorkAreas(Integer codeCompany) throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"code\",T0.\"name\" FROM \"WorkArea\" T0 where T0.\"codeCompany\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ItemInformation us = new ItemInformation();
				us.setItemCode(rs.getString("code"));
				us.setDscription(rs.getString("name"));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {

		}
		return datos;
	}

	public static List<ItemInformation> getEmployeeComponent(Integer codeCompany)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT T0.\"idEmployee\",T0.\"name\" FROM \"employee\" T0 INNER JOIN \"WorkPosition\" T1 ON T0.\"codeCompany\" = T1.\"codeCompany\" AND T0.\"codeWorkPosition\" = T1.\"code\" AND T0.\"codeWorkArea\" = T1.\"codeWorkArea\" WHERE T0.\"codeCompany\" = ? and T1.\"headArea\" = 1;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, codeCompany);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ItemInformation us = new ItemInformation();
				us.setItemCode(rs.getString("idEmployee"));
				us.setDscription(rs.getString("name"));
				datos.add(us);
				datos.size();
			}
		} catch (SQLException e) {

		}
		return datos;
	}

	public static boolean deleteWorkPosition(WorkPositionInformation work) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete from \"WorkPosition\" where \"code\" = ? and \"codeCompany\"= ? and codeWorkArea = ?";
		try {
			ps = con.prepareStatement(query);
			ps.setString(1, work.getCode());
			ps.setInt(2, work.getCodeCompany());
			ps.setString(3, work.getCodeWorkArea());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Delete workArea failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean deleteEmployee(Integer codeCompany, Integer code)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete from \"Employee\" where \"idEmployee\" = ? and \"codeCompany\"= ?;";
		try {
			ps = con.prepareStatement(query);
			ps.setInt(1, code);
			ps.setInt(2, codeCompany);

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Delete employee failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static boolean deleteFixed(Integer codeCompany, Integer code) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete from \"FixedAsset\" where \"idAsset\" = ? and \"codeCompany\"= ?;";
		try {
			ps = con.prepareStatement(query);
			ps.setInt(1, code);
			ps.setInt(2, codeCompany);

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				ps.close();
				con.close();
				throw new SQLException("Delete fixed failed, no rows affected.");
			} else {
				ps.close();
				con.close();
				return true;
			}
		} catch (Exception e) {
			ps.close();
			con.close();
			return false;
		}
	}

	public static List<ItemInformation> getWorkAreaCompany(Integer company)
			throws ClassNotFoundException, SQLException {
		String sql = "select \"code\", \"name\" from \"WorkArea\" where \"codeCompany\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, company);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ItemInformation us = new ItemInformation();
				us.setItemCode(rs.getString(1));
				us.setDscription(rs.getString(2));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ComboElement> getWorkPositionCompany(Integer company, String code)
			throws ClassNotFoundException, SQLException {
		String sql = "select \"code\",\"name\" from \"WorkPosition\" where \"codeCompany\" = ? and \"codeWorkArea\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ComboElement> datos = new ArrayList<ComboElement>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, company);
		pst.setString(2, code);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ComboElement us = new ComboElement();
				us.setCode(rs.getString(1));
				us.setDescription(rs.getString(2));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ComboElement> getAreaCmbCompany(Integer company) throws ClassNotFoundException, SQLException {
		String sql = "select \"code\",\"name\" from \"WorkArea\" where \"codeCompany\" = ?;";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ComboElement> datos = new ArrayList<ComboElement>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, company);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				ComboElement us = new ComboElement();
				us.setCode(rs.getString(1));
				us.setDescription(rs.getString(2));
				datos.add(us);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ItemInformation> viewAsset() throws ClassNotFoundException, SQLException {

		String sql = "SELECT idAsset,description FROM FixedAsset";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String idAsset = rs.getString(1);
				String description = rs.getString(2);
				ItemInformation item = new ItemInformation(idAsset, description);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ItemInformation> viewAssetEmployee() throws ClassNotFoundException, SQLException {

		String sql = "SELECT \"idEmployee\",\"name\" FROM \"Employee\"";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String idEmployee = rs.getString(1);
				String name = rs.getString(2);
				ItemInformation item = new ItemInformation(idEmployee, name);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<MaintenanceInformation> viewtableAssetMaintenance(Integer idAsset) {
		String sql = "SELECT IdMaintenance ,date,dateCreated , idEmployee,userCreated,userModificated FROM Maintenance Where idAsset = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<MaintenanceInformation> datos = new ArrayList<MaintenanceInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idAsset);
			rs = pst.executeQuery();

			while (rs.next()) {
				MaintenanceInformation asset = new MaintenanceInformation();
				asset.setIdMaintenance(rs.getInt(1));
				asset.setDate(rs.getDate(2));
				asset.setDateCreated(rs.getDate(3));
				asset.setIdEmployee(rs.getInt(4));
				asset.setUserCreated(rs.getString(5));
				asset.setUserModificated(rs.getString(6));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static boolean addAssetMaintenance(List<MaintenanceInformation> maintenance)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		MaintenanceInformation us = new MaintenanceInformation();

		for (MaintenanceInformation assetsInformation : maintenance) {

			if (assetsInformation.getIdMaintenance() == null) {

				String sql = "INSERT INTO \"Maintenance\" (\"idAsset\",\"codeCompany\",\"idEmployee\",\"description\",\"date\",\"userCreated\",\"dateCreated\",\"commentary\") VALUES (?,?,?,?,?,?,?,?)";
				try {
					ps = con.prepareStatement(sql);
					ps.setInt(1, assetsInformation.getIdAsset());
					ps.setInt(2, assetsInformation.getCodeCompany());
					ps.setInt(3, assetsInformation.getIdEmployee());
					ps.setString(4, assetsInformation.getDescription());
					ps.setTimestamp(5, new Timestamp(assetsInformation.getDate().getTime()));
					ps.setString(6, assetsInformation.getUserCreated());
					ps.setTimestamp(7, new Timestamp(assetsInformation.getDateCreated().getTime()));
					ps.setString(8, assetsInformation.getCommentary());

					int rowsaffect = ps.executeUpdate();
					if (rowsaffect == 0) {
						throw new SQLException("Creating Asset failed, no rows affected.");
					}
				} catch (Exception e) {
					return false;
				}
			} else {
				UpgradeMaintenance(assetsInformation);
			}
		}

		return true;
	}

	public static boolean UpgradeMaintenance(MaintenanceInformation assetsInformation)
			throws ClassNotFoundException, SQLException {
		boolean modificateRegister = false;
		MaintenanceInformation maintenanceInformation = viewtableAssetForIdMaintenance(
				assetsInformation.getIdMaintenance());

		if (maintenanceInformation != null) {

			if (assetsInformation.getDate() != null
					&& !assetsInformation.getDate().equals(maintenanceInformation.getDate())) {
				modificateRegister = true;
			}
			if (assetsInformation.getDescription() != null
					&& !assetsInformation.getDescription().equals(maintenanceInformation.getDescription())) {
				modificateRegister = true;
			}
			if (assetsInformation.getCodeCompany() != null
					&& !assetsInformation.getCommentary().equals(maintenanceInformation.getCommentary())) {
				modificateRegister = true;
			}

			if (modificateRegister) {
				modificationAssetmaintenance(assetsInformation);
			}

		}

		return false;

	}

	public static MaintenanceInformation viewtableAssetForIdMaintenance(Integer IdMaintenance) {
		String sql = "SELECT IdMaintenance ,IdAsset,date,dateCreated , idEmployee,userCreated,userModificated FROM Maintenance Where IdMaintenance = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<MaintenanceInformation> datos = new ArrayList<MaintenanceInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdMaintenance);
			rs = pst.executeQuery();

			while (rs.next()) {
				MaintenanceInformation asset = new MaintenanceInformation();
				asset.setIdMaintenance(rs.getInt(1));
				asset.setIdAsset(rs.getInt(2));
				asset.setDate(rs.getDate(3));
				asset.setDateCreated(rs.getDate(4));
				asset.setIdEmployee(rs.getInt(5));
				asset.setUserCreated(rs.getString(6));
				asset.setUserModificated(rs.getString(7));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos != null ? datos.get(0) : null;
	}

	public static boolean modificationAssetmaintenance(MaintenanceInformation assetsInformation)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();

		String sql = "UPDATE  Maintenance SET description = ?, date =? ,userModificated =?,commentary =?  WHERE IdMaintenance = ?";
		try {
			ps = con.prepareStatement(sql);
			if (assetsInformation.getDescription() != null) {
				ps.setString(1, assetsInformation.getDescription());
			}
			if (assetsInformation.getDate() != null) {
				ps.setTimestamp(2, new Timestamp(assetsInformation.getDate().getTime()));
			}
			ps.setString(3, assetsInformation.getUserModificated());

//			ps.setTimestamp(4, new Timestamp(assetsInformation.getDateModificated().getTime()));
			if (assetsInformation.getCommentary() != null) {
				ps.setString(4, assetsInformation.getCommentary());
			}
			ps.setInt(5, assetsInformation.getIdMaintenance());
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("modificated	 Asset failed, no rows affected.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean deleteMaintenance(MaintenanceInformation maintenance)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete  from Maintenance where idMaintenance = ?";
		try {

			ps = con.prepareStatement(query);
			ps.setInt(1, maintenance.getIdMaintenance());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Delete user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static List<UpgradeInformation> viewtableUpgrade(Integer idAsset)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT IdUpgrade ,date,dateCreated , idEmployee,userCreated,userModificated FROM Upgrade Where idAsset = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<UpgradeInformation> datos = new ArrayList<UpgradeInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, idAsset);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				UpgradeInformation asset = new UpgradeInformation();
				asset.setIdUpgrade(rs.getInt(1));
				asset.setDate(rs.getDate(2).toLocalDate());
				asset.setDateCreated(rs.getDate(3));
				asset.setIdEmployee(rs.getInt(4));
				asset.setUserCreated(rs.getString(5));
				asset.setUserModificated(rs.getString(6));
				datos.add(asset);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static boolean addAssetUpgrade(List<UpgradeInformation> upgrade)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		UpgradeInformation us = new UpgradeInformation();

		for (UpgradeInformation assetsInformation : upgrade) {

			if (assetsInformation.getIdUpgrade() == null) {

				String sql = "INSERT INTO \"Upgrade\" (\"idAsset\",\"codeCompany\",\"idEmployee\",\"description\",\"date\",\"userCreated\",\"dateCreated\",\"commentary\") VALUES (?,?,?,?,?,?,?,?)";
				try {
					ps = con.prepareStatement(sql);
					ps.setInt(1, assetsInformation.getIdAsset());
					ps.setInt(2, assetsInformation.getCodeCompany());
					ps.setInt(3, assetsInformation.getIdEmployee());
					ps.setString(4, assetsInformation.getDescription());
					ps.setDate(2, Date.valueOf(assetsInformation.getDate()));
					ps.setString(6, assetsInformation.getUserCreated());
					ps.setTimestamp(7, new Timestamp(assetsInformation.getDateCreated().getTime()));
					ps.setString(8, assetsInformation.getCommentary());

					int rowsaffect = ps.executeUpdate();
					if (rowsaffect == 0) {
						throw new SQLException("Creating Asset failed, no rows affected.");
					}
				} catch (Exception e) {
					return false;
				}
			} else {
				UpgradeAssetUpgrade(assetsInformation);
			}
		}

		return true;
	}

	public static boolean UpgradeAssetUpgrade(UpgradeInformation assetsInformation)
			throws ClassNotFoundException, SQLException {
		boolean modificateRegister = false;
		UpgradeInformation upgradeasset = viewtableAssetForIdUpgrade(assetsInformation.getIdUpgrade());

		if (upgradeasset != null) {

			if (assetsInformation.getDate() != null && !assetsInformation.getDate().equals(upgradeasset.getDate())) {
				modificateRegister = true;
			}
			if (assetsInformation.getDescription() != null
					&& !assetsInformation.getDescription().equals(upgradeasset.getDescription())) {
				modificateRegister = true;
			}
			if (assetsInformation.getCodeCompany() != null
					&& !assetsInformation.getCommentary().equals(upgradeasset.getCommentary())) {
				modificateRegister = true;
			}

			if (modificateRegister) {
				modificationAssetUpgrade(assetsInformation);
			}

		}

		return false;

	}

	public static boolean modificationAssetUpgrade(UpgradeInformation assetsInformation)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		UpgradeInformation us = new UpgradeInformation();

		String sql = "UPDATE  Upgrade SET description = ?, date =? ,userModificated =?,commentary =?  WHERE IdUpgrade = ?";
		try {
			ps = con.prepareStatement(sql);
			if (assetsInformation.getDescription() != null) {
				ps.setString(1, assetsInformation.getDescription());
			}
			if (assetsInformation.getDate() != null) {
				ps.setDate(2, Date.valueOf(assetsInformation.getDate()));
			}
			ps.setString(3, assetsInformation.getUserModificated());
//			ps.setTimestamp(4, new Timestamp(assetsInformation.getDateModificated().getTime()));
			if (assetsInformation.getCommentary() != null) {
				ps.setString(4, assetsInformation.getCommentary());
			}
			ps.setInt(5, assetsInformation.getIdUpgrade());
			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("modificated	 Asset failed, no rows affected.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static UpgradeInformation viewtableAssetForIdUpgrade(Integer IdUpgrade) {
		String sql = "SELECT IdUpgrade ,IdAsset,date,dateCreated , idEmployee,userCreated,userModificated FROM Upgrade Where IdUpgrade = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<UpgradeInformation> datos = new ArrayList<UpgradeInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdUpgrade);
			rs = pst.executeQuery();

			while (rs.next()) {
				UpgradeInformation asset = new UpgradeInformation();
				asset.setIdUpgrade(rs.getInt(1));
				asset.setIdAsset(rs.getInt(2));
				asset.setDate(rs.getDate(3).toLocalDate());
				asset.setDateCreated(rs.getDate(4));
				asset.setIdEmployee(rs.getInt(5));
				asset.setUserCreated(rs.getString(6));
				asset.setUserModificated(rs.getString(7));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos != null ? datos.get(0) : null;
	}

	public static List<ItemInformation> viewAssetDown() throws ClassNotFoundException, SQLException {

		String sql = "SELECT idEmployee, name FROM Employee ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {

				String idAsset = rs.getString(1);
				String description = rs.getString(2);
				ItemInformation item = new ItemInformation(idAsset, description);
				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ItemInformation> viewAssetEmployeeDown() throws ClassNotFoundException, SQLException {

		String sql = "SELECT \"idEmployee\",\"name\" FROM \"Employee\"";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String idEmployee = rs.getString(1);
				String name = rs.getString(2);
				ItemInformation item = new ItemInformation(idEmployee, name);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ItemInformation> viewAssetSeriedown() throws ClassNotFoundException, SQLException {

		String sql = "SELECT \"board\",\"serialNumber\" FROM \"FixedAsset\"";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String board = rs.getString(1);
				String serialNumber = rs.getString(2);
				ItemInformation item = new ItemInformation(board, serialNumber);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<ItemInformation> viewAssetReport() throws ClassNotFoundException, SQLException {

		String sql = "SELECT \"idAsset\",\"description\" FROM \"FixedAsset\"";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String idAsset = rs.getString(1);
				String description = rs.getString(2);
				ItemInformation item = new ItemInformation(idAsset, description);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static List<AssetDownInformation> viewtableAssetDown(Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = "SELECT idAsset ,board,description,status  FROM FixedAsset Where idEmployee = ? AND status = 'A'";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<AssetDownInformation> datos = new ArrayList<AssetDownInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, idEmployee);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				AssetDownInformation asset = new AssetDownInformation();
				asset.setIdAsset(rs.getInt(1));
				asset.setBoard(rs.getString(2));
				asset.setDescription(rs.getString(3));
				asset.setStatus(rs.getString(4));
				datos.add(asset);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static boolean addAssetDown(List<AssetDownInformation> returnAsset)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		AssetDownInformation us = new AssetDownInformation();

		if (returnAsset != null && !returnAsset.isEmpty()) {

			AssetDownInformation assetsInformation = returnAsset.get(0);

			if (assetsInformation.getIdDownAsset() == null) {

				String sql = "INSERT INTO \"DownAsset\" (\"idAsset\",\"codeCompany\",\"idEmployee\",\"description\",\"date\",\"userCreated\",\"dateCreated\") VALUES (?,?,?,?,?,?,?)";
				try {
					ps = con.prepareStatement(sql);
					ps.setInt(1, assetsInformation.getIdAsset());
					ps.setInt(2, assetsInformation.getCodeCompany());
					ps.setInt(3, assetsInformation.getIdEmployee());
					ps.setString(4, assetsInformation.getDescription());
					ps.setTimestamp(5, new Timestamp(assetsInformation.getDate().getTime()));
					ps.setString(6, assetsInformation.getUserCreated());
					ps.setTimestamp(7, new Timestamp(assetsInformation.getDateCreated().getTime()));

					int rowsaffect = ps.executeUpdate();
					if (rowsaffect == 0) {
						throw new SQLException("Creating Asset failed, no rows affected.");
					}

					UpdateDownStatus(assetsInformation.getIdAsset(), assetsInformation.getCodeCompany());

				} catch (Exception e) {
					return false;
				}
			}
		}

		return true;
	}

	public static AssetDownInformation viewtableAssetForIdDownAsset(Integer IdDownAsset) {
		String sql = "SELECT IdDownAsset ,IdAsset,date,dateCreated , idEmployee,userCreated,userModificated,status FROM DownAsset Where IdDownAsset = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<AssetDownInformation> datos = new ArrayList<AssetDownInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdDownAsset);
			rs = pst.executeQuery();

			while (rs.next()) {
				AssetDownInformation asset = new AssetDownInformation();
				asset.setIdDownAsset(rs.getInt(1));
				asset.setIdAsset(rs.getInt(2));
				asset.setDate(rs.getDate(3));
				asset.setDateCreated(rs.getDate(4));
				asset.setIdEmployee(rs.getInt(5));
				asset.setUserCreated(rs.getString(6));
				asset.setUserModificated(rs.getString(7));
				asset.setStatus(rs.getString(8));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos != null ? datos.get(0) : null;
	}

	public static boolean deleteDownAsset(AssetDownInformation downasset) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete  from DownAsset where idDownAsset = ?";
		try {

			ps = con.prepareStatement(query);
			ps.setInt(1, downasset.getIdDownAsset());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Delete user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean UpdateDownStatus(Integer IdAsset, Integer codeCompany) {
		String sql = "UPDATE FixedAsset SET status = 'B' WHERE idAsset = ?  AND codecompany = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		try {
			AssetDownInformation assetD = new AssetDownInformation();
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdAsset);
			pst.setInt(3, codeCompany);
			rs = pst.executeQuery();

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;

	}

	public static boolean addAssetDowns(List<AssetDownInformation> assetdown)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		AssetDownInformation us = new AssetDownInformation();

		for (AssetDownInformation assetsInformation : assetdown) {

			if (assetsInformation.getIdDownAsset() == null) {

				String sql = "INSERT INTO \"Maintenance\" (\"idAsset\",\"codeCompany\",\"idEmployee\",\"description\",\"date\",\"userCreated\",\"dateCreated\",\"commentary\") VALUES (?,?,?,?,?,?,?,?)";
				try {
					ps = con.prepareStatement(sql);
					ps.setInt(1, assetsInformation.getIdAsset());
					ps.setInt(2, assetsInformation.getCodeCompany());
					ps.setInt(3, assetsInformation.getIdEmployee());
					ps.setString(4, assetsInformation.getDescription());
					ps.setTimestamp(5, new Timestamp(assetsInformation.getDate().getTime()));
					ps.setString(6, assetsInformation.getUserCreated());
					ps.setTimestamp(7, new Timestamp(assetsInformation.getDateCreated().getTime()));
					ps.setString(8, assetsInformation.getCommentary());

					int rowsaffect = ps.executeUpdate();
					if (rowsaffect == 0) {
						throw new SQLException("Creating Asset failed, no rows affected.");
					}
				} catch (Exception e) {
					return false;
				}
			}
		}

		return true;
	}

	public static boolean addAssetReturn(List<AssetRetunrInformation> returnAsset)
			throws ClassNotFoundException, SQLException {
		Integer idAsset = null;
		PreparedStatement ps = null;
		Connection con = connection();
		AssetRetunrInformation us = new AssetRetunrInformation();

		if (returnAsset != null && !returnAsset.isEmpty()) {
			AssetRetunrInformation assetsInformation = returnAsset.get(0);

			idAsset = assetsInformation.getIdAsset();

			if (assetsInformation.getIdDevolution() == null) {

				String sql = "INSERT INTO \"Devolution\" (\"idAsset\",\"codeCompany\",\"idEmployee\",\"description\",\"date\",\"userCreated\",\"dateCreated\",\"commentary\") VALUES (?,?,?,?,?,?,?,?)";
				try {
					ps = con.prepareStatement(sql);
					ps.setInt(1, assetsInformation.getIdAsset());
					ps.setInt(2, assetsInformation.getCodeCompany());
					ps.setInt(3, assetsInformation.getIdEmployee());
					ps.setString(4, assetsInformation.getDescription());
					ps.setTimestamp(5, new Timestamp(assetsInformation.getDate().getTime()));
					ps.setString(6, assetsInformation.getUserCreated());
					ps.setTimestamp(7, new Timestamp(assetsInformation.getDateCreated().getTime()));
					ps.setString(8, assetsInformation.getCommentary());

					int rowsaffect = ps.executeUpdate();
					if (rowsaffect == 0) {
						throw new SQLException("Creating Asset failed, no rows affected.");
					}
					Integer idEmployee = employeeWorkStation(idAsset);
					UpdateDevolutionStatus(idAsset, assetsInformation.getCodeCompany(), idEmployee);

//							UpdateEmployeeAsset(idAsset, idEmployee);
				} catch (Exception e) {
					return false;
				}
			}
		}

		return true;
	}

	public static boolean UpdateDevolutionStatus(Integer IdAsset, Integer codeCompany, Integer idEmployee) {
		String sql = "UPDATE FixedAsset SET status = 'L', idEmployee = " + idEmployee
				+ "WHERE idAsset = ?  AND codecompany = ? ";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		try {
			AssetRetunrInformation assetD = new AssetRetunrInformation();
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdAsset);
			pst.setInt(2, codeCompany);

			pst.executeUpdate();

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;

	}

	public static Integer employeeWorkStation(Integer idAsset) {

		Integer idEmploye = 0;

		String sql = "select		    b.idEmployee" + "	  from			    Employee b," + "				("
				+ "						select			b.codeWorkArea		"
				+ "						from			FixedAsset a, "
				+ "										Employee b" + "						where			idAsset = ?"
				+ "						and				a.idEmployee = b.idEmployee" + "				) as temp1,"
				+ "				(" + "					select		    code,"
				+ "									codeWorkArea" + "					from			workposition b"
				+ "					where			b.headArea = 1" + "				) as temp2"
				+ " where           b.codeWorkPosition = temp2.code"
				+ " and				temp1.codeWorkArea = temp2.codeWorkArea";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idAsset);
			rs = pst.executeQuery();

			while (rs.next()) {
				idEmploye = rs.getInt(1);
			}

		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
		return idEmploye;
	}

	public static boolean deleteDevolution(AssetRetunrInformation returnAsset)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		Connection con = connection();
		String query = "delete  from Devolution where idDevolution = ?";
		try {

			ps = con.prepareStatement(query);
			ps.setInt(1, returnAsset.getIdDevolution());

			int rowsaffect = ps.executeUpdate();
			if (rowsaffect == 0) {
				throw new SQLException("Delete user failed, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static List<AssetRetunrInformation> viewtableAssetDevolution(Integer idAsset) {
		String sql = "SELECT IdDevolution ,date,dateCreated , idEmployee,userCreated,userModificated FROM Devolution Where idAsset = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<AssetRetunrInformation> datos = new ArrayList<AssetRetunrInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idAsset);
			rs = pst.executeQuery();

			while (rs.next()) {
				AssetRetunrInformation asset = new AssetRetunrInformation();
				asset.setIdDevolution(rs.getInt(1));
				asset.setDate(rs.getDate(2));
				asset.setDateCreated(rs.getDate(3));
				asset.setIdEmployee(rs.getInt(4));
				asset.setUserCreated(rs.getString(5));
				asset.setUserModificated(rs.getString(6));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}

	public static AssetRetunrInformation viewtableAssetForIdDevolution(Integer IdDevolution) {
		String sql = "SELECT IdDevolution ,IdAsset,date,dateCreated , idEmployee,userCreated,userModificated FROM Devolution Where IdDevolution = ?";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<AssetRetunrInformation> datos = new ArrayList<AssetRetunrInformation>();

		try {

			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, IdDevolution);
			rs = pst.executeQuery();

			while (rs.next()) {
				AssetRetunrInformation asset = new AssetRetunrInformation();
				asset.setIdDevolution(rs.getInt(1));
				asset.setIdAsset(rs.getInt(2));
				asset.setDate(rs.getDate(3));
				asset.setDateCreated(rs.getDate(4));
				asset.setIdEmployee(rs.getInt(5));
				asset.setUserCreated(rs.getString(6));
				asset.setUserModificated(rs.getString(7));
				datos.add(asset);
				datos.size();

			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos != null ? datos.get(0) : null;
	}

	public static List<ReportMaintenance> ReportMaintenance(Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = " select  T0.dateCreated , T2.name, T2.lastName, T1.description , T0.description  ,T3.name,T1.board,T4.name"
				+ " from Maintenance T0 "
				+ " inner join FixedAsset T1  on T0.idAsset = T1.idAsset and T0.codeCompany = T1.codeCompany"
				+ " inner join Employee T2 on T0.idEmployee = T2.idEmployee and T0.codeCompany = T2.codeCompany "
				+ " inner join Company T3 on T0.codeCompany = T3.codeCompany "
				+ " inner join WorkPosition T4 on T0.codeCompany = T4.codeCompany  and T2.codeWorkArea = T4.codeWorkArea and T2.codeWorkPosition = T4.code"
				+ " where T1.idEmployee = ? ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<ReportMaintenance> datos = new ArrayList<ReportMaintenance>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idEmployee);
			rs = pst.executeQuery();
			while (rs.next()) {

				ReportMaintenance us = new ReportMaintenance();

				us.setDate(rs.getDate(1));
				us.setNameEmployee(rs.getString(2));
				us.setLastEmployee(rs.getString(3));
				us.setDescription(rs.getString(4));
				us.setDescriptionm(rs.getString(5));
				us.setNameCompany(rs.getString(6));
				us.setBoard(rs.getString(7));
				us.setNameArea(rs.getString(8));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());
				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	public static List<reportUpgrade> ReportUpgrade(Integer idEmployee) throws ClassNotFoundException, SQLException {
		String sql = " Select  T0.dateCreated,T2.name,T2.lastname,T1.description,T0.description, T3.name,T1.board,T4.name "
				+ "  from Upgrade T0 "
				+ " inner join FixedAsset T1  on T0.idAsset = T1.idAsset and T0.codeCompany = T1.codeCompany "
				+ " inner join Employee T2 on T0.idEmployee = T2.idEmployee and T0.codeCompany = T2.codeCompany "
				+ " inner join Company T3 on T0.codeCompany = T3.codeCompany"
				+ " inner join WorkPosition T4 on T0.codeCompany = T4.codeCompany  and T2.codeWorkArea = T4.codeWorkArea and T2.codeWorkPosition = T4.code "
				+ "  where T1.idEmployee = ? ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<reportUpgrade> datos = new ArrayList<reportUpgrade>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idEmployee);
			rs = pst.executeQuery();
			while (rs.next()) {

				reportUpgrade us = new reportUpgrade();

				us.setDate(rs.getDate(1));
				us.setNameEmployee(rs.getString(2));
				us.setLastEmployee(rs.getString(3));
				us.setDescription(rs.getString(4));
				us.setDescriptionm(rs.getString(5));
				us.setNameCompany(rs.getString(6));
				us.setBoard(rs.getString(7));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());
				us.setNameArea(rs.getString(8));

				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	public static List<reportRetunr> ReportReturn(Integer idEmployee, Integer employee)
			throws ClassNotFoundException, SQLException {
		String sql = " select  T0.date,T2.name,T2.lastName,T1.description,T1.board,T0.description,T3.name,T4.name "
				+ " from Devolution T0  " + " inner join FixedAsset T1  on T0.idAsset = T1.idAsset "
				+ " inner join Employee T2 on T0.codeCompany = T2.codeCompany   "
				+ " inner join Company T3 on T0.codeCompany = T3.codeCompany "
				+ " inner join WorkPosition T4 on T2.codeWorkPosition = T4.code and T2.codeCompany = T4.codeCompany"
				+ "  where T0.idEmployee = ? and T2.idEmployee = ?";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<reportRetunr> datos = new ArrayList<reportRetunr>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idEmployee);
			pst.setInt(2, employee);
			rs = pst.executeQuery();
			while (rs.next()) {

				reportRetunr us = new reportRetunr();

				us.setDate(rs.getDate(1));
				us.setNameEmployee(rs.getString(2));
				us.setLastName(rs.getString(3));
				us.setDescription(rs.getString(4));
				us.setBoard(rs.getString(5));
				us.setDescriptionm(rs.getString(6));
				us.setNameCompany(rs.getString(7));
				us.setNameArea(rs.getString(8));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());
				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	public static List<reportAsset> ReportAsset(Integer idAsset) throws ClassNotFoundException, SQLException {
		String sql = "select T0.description, T0.board,T2.name, T0.dateCreated ,T1.name, T1.lastName "
				+ " from FixedAsset T0   " + " inner join Employee T1 on T0.idEmployee = T1.idEmployee "
				+ " inner join Company T2 on T0.codeCompany = T2.codeCompany " + "  where T0.idAsset = ? ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<reportAsset> datos = new ArrayList<reportAsset>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idAsset);
			rs = pst.executeQuery();
			while (rs.next()) {

				reportAsset us = new reportAsset();

				us.setNameAsset(rs.getString(1));
				us.setBoard(rs.getString(2));
				us.setNameCompany(rs.getString(3));
				us.setDate(rs.getDate(4));
				us.setNameEmployee(rs.getString(5));
				us.setLastEmployee(rs.getString(6));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());
//				us.setDescription(rs.getString(4));
//				us.setDescriptionm(rs.getString(5));

				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	public static List<reportDelivery> ReporPrincipalDelivery(Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = "select T3.name,T3.lastName,T5.name,T4.name,T4.nit,T2.comments,T0.board,T0.serialNumber,T0.model,T1.cant,T3.name,T3.lastName,T5.name,T0.frontPicture,T0.backPicture "
				+ " from FixedAsset T0 "
				+ " inner join DeliveryCertificate_Det T1 on T0.idAsset = T1.idAsset  and T1.codeCompany = T0.codeCompany  "
				+ "	inner join DeliveryCertificate_Enc T2 on T0.idEmployee = T2.idEmployee and T1.idDelivery_det = T2.idDelivery "
				+ " inner join Employee T3 on  T0.idEmployee = T3.idEmployee  "
				+ " inner join Company T4 on T0.codeCompany = T4.codeCompany "
				+ " inner join WorkPosition T5 on T3.codeWorkArea = T5.codeWorkArea and T3.codeWorkPosition = T5.code  "
				+ "  where T0.idEmployee = ? and T5.headArea = 1 ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<reportDelivery> datos = new ArrayList<reportDelivery>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idEmployee);
			rs = pst.executeQuery();
			while (rs.next()) {

				reportDelivery us = new reportDelivery();

				us.setNameEmployee(rs.getString(1));
				us.setLastEmployee(rs.getString(2));
				us.setNameArea(rs.getString(3));
				us.setNameCompany(rs.getString(4));
				us.setCompanyNit(rs.getString(5));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());
				us.setDescription(rs.getString(6));
				us.setBoard(rs.getString(7));
				us.setSerie(rs.getString(9));
				us.setModel(rs.getString(9));
				us.setCant(rs.getInt(10));
				us.setNameBoss(rs.getString(11));
				us.setLastBoss(rs.getString(12));
				us.setAreaBoss(rs.getString(13));
				us.setPhoto(rs.getBytes(14));
				us.setPhotoBack(rs.getBytes(15));

				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	public static List<ReportDeliveryAssetTwo> ReportSubreport(Integer idEmployee)
			throws ClassNotFoundException, SQLException {
		String sql = "select T4.name,T4.nit,T3.name,T3.lastname,T5.name,T0.board,T0.frontPicture,T0.backPicture "
				+ " from FixedAsset T0 "
				+ " inner join DeliveryCertificate_Det T1 on T0.idAsset = T1.idAsset  AND T1.codeCompany = T0.codeCompany "
				+ " inner join DeliveryCertificate_Enc T2 on T1.idDelivery = T2.idDelivery and T0.idEmployee = T2.idEmployee "
				+ " inner join Employee T3 on  T0.idEmployee = T3.idEmployee "
				+ " inner join Company T4 on T0.codeCompany = T4.codeCompany "
				+ " inner join WorkPosition T5 on T3.codeWorkArea = T5.codeWorkArea and T3.codeWorkPosition = T5.code "
				+ "  where T0.idEmployee = ?  ";

		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;

		List<ReportDeliveryAssetTwo> datos = new ArrayList<ReportDeliveryAssetTwo>();

		try {
			con = connection();
			pst = con.prepareStatement(sql);
			pst.setInt(1, idEmployee);
			rs = pst.executeQuery();
			while (rs.next()) {

				ReportDeliveryAssetTwo us = new ReportDeliveryAssetTwo();

				us.setNameCompany(rs.getString(1));
				us.setCompanyNit(rs.getString(2));
				us.setNameEmployee(rs.getString(3));
				us.setLastEmployee(rs.getString(4));
				us.setNameArea(rs.getString(5));
				us.setBoard(rs.getString(6));
				us.setPhoto(rs.getBytes(7));
				us.setPhotoBack(rs.getBytes(8));
				us.setDayDate(dayDate());
				us.setMonthDate(monthDate());
				us.setYearDate(yearDate());

				datos.add(us);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;

	}

	
	public static List<ItemInformation> viewAssetEmployeeOptionDelivery() throws ClassNotFoundException, SQLException {

		String sql = "SELECT \"idEmployee\",\"name\" FROM \"Employee\"";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<ItemInformation> datos = new ArrayList<ItemInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				String idEmployee = rs.getString(1);
				String name = rs.getString(2);
				ItemInformation item = new ItemInformation(idEmployee, name);

				datos.add(item);
				datos.size();

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "ocurrio un error" + e.getMessage());
		}
		return datos;
	}
	
	
	
	public static List<DeliveryDetInformation> searchEmployee(Integer idEmployee , Integer codeCompany) throws ClassNotFoundException, SQLException {
		String sql = "SELECT board,model,description FROM FixedAsset WHERE   idEmployee = ?  and codeCompany= ? ";
		Connection con;
		PreparedStatement pst;
		ResultSet rs = null;
		List<DeliveryDetInformation> datos = new ArrayList<DeliveryDetInformation>();
		con = connection();
		pst = con.prepareStatement(sql);
		pst.setInt(1, idEmployee);
		pst.setInt(2, codeCompany);
		rs = pst.executeQuery();
		try {

			while (rs.next()) {
				DeliveryDetInformation us = new DeliveryDetInformation();
				us.setAssetBoard(rs.getString("board"));
				us.setAssetModel(rs.getString("model"));
				us.setAssetName(rs.getString("description"));
				datos.add(us);
			}
		} catch (SQLException e) {
		}
		return datos;
	}
	
	
	
	
	
	
	
	public static String dayDate() {

		java.util.Date fecha = new java.util.Date();
		SimpleDateFormat formatodia = new SimpleDateFormat("dd");
		return formatodia.format(fecha);

	}

	public static String monthDate() {

		java.util.Date fecha = new java.util.Date();
		SimpleDateFormat formatomes = new SimpleDateFormat("MM");
		return formatomes.format(fecha);

	}

	public static String yearDate() {

		java.util.Date fecha = new java.util.Date();
		SimpleDateFormat formatoano = new SimpleDateFormat("YYYY");
		return formatoano.format(fecha);

	}

	public static String Encriptar(String texto) {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

			SecretKey key = new SecretKeySpec(keyBytes, "DESede");
			Cipher cipher = Cipher.getInstance("DESede");
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] plainTextBytes = texto.getBytes("utf-8");
			byte[] buf = cipher.doFinal(plainTextBytes);
			byte[] base64Bytes = Base64.encodeBase64(buf);
			base64EncryptedString = new String(base64Bytes);

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}

	public static String Desencriptar(String textoEncriptado) throws Exception {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {
			byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			SecretKey key = new SecretKeySpec(keyBytes, "DESede");

			Cipher decipher = Cipher.getInstance("DESede");
			decipher.init(Cipher.DECRYPT_MODE, key);

			byte[] plainText = decipher.doFinal(message);

			base64EncryptedString = new String(plainText, "UTF-8");

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}

	public static String base64(String txt) {
		return FixedAssetDB.Encriptar(txt);
	}

}
