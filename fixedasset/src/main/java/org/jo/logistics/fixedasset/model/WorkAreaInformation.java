package org.jo.logistics.fixedasset.model;

public class WorkAreaInformation {
	
	private Integer codeCompany;
	private String code;
	private String name;
	private String description;
	
	
	public WorkAreaInformation() {
		super();
		// TODO Auto-generated constructor stub
	}


	public WorkAreaInformation(Integer codeCompany, String code, String name, String description) {
		super();
		this.codeCompany = codeCompany;
		this.code = code;
		this.name = name;
		this.description = description;
	}


	public Integer getCodeCompany() {
		return codeCompany;
	}


	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

}
