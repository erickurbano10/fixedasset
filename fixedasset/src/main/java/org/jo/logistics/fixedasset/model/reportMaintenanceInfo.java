package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class reportMaintenanceInfo {

	private Integer idMaintenace;
	private String nameEmployee;
	private String nameAsset;
	private String description; 
	private String commentary;
	private Date  date ;
	
	public reportMaintenanceInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public reportMaintenanceInfo(Integer idMaintenace, String nameEmployee, String nameAsset, String description,
			String commentary, Date date) {
		super();
		this.idMaintenace = idMaintenace;
		this.nameEmployee = nameEmployee;
		this.nameAsset = nameAsset;
		this.description = description;
		this.commentary = commentary;
		this.date = date;
	}


	public Integer getIdMaintenace() {
		return idMaintenace;
	}
	public void setIdMaintenace(Integer idMaintenace) {
		this.idMaintenace = idMaintenace;
	}
	public String getNameEmployee() {
		return nameEmployee;
	}
	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}
	public String getNameAsset() {
		return nameAsset;
	}
	public void setNameAsset(String nameAsset) {
		this.nameAsset = nameAsset;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	
	
	
	
}
