package org.jo.logistics.fixedasset.view.ui;


import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 
 * @author Fabian Andres Caciedo Cuellar
 *
 */
public class ProgressIndicatorSceneTask extends Task<Void> {
	private Stage dialogStage;
	private HBox hb;
	private ProgressIndicator pin;

	public ProgressIndicatorSceneTask() {
		dialogStage = new Stage();
		dialogStage.initStyle(StageStyle.TRANSPARENT);
		dialogStage.setResizable(false);
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		Float style = new Float(-1.0f);
		pin = new ProgressIndicator();
		pin.setMinWidth(280);
		pin.setMinHeight(280);
		pin.setProgress(style);
		hb = new HBox();
		hb.setSpacing(5);
		hb.setAlignment(Pos.CENTER);
		hb.getChildren().addAll(pin);
	}

	@Override
	protected void running() {
		super.running();
		StackPane layout = new StackPane();
		layout.getChildren().setAll(hb);
		layout.setStyle("-fx-background-color: null");
		Scene scene = new Scene(layout, Color.TRANSPARENT);
		dialogStage.setScene(scene);
		dialogStage.show();
	}

	@Override
	protected void cancelled() {
		super.cancelled();
		dialogStage.hide();
	}

	@Override
	protected void failed() {
		super.failed();
		dialogStage.hide();
	}

	@Override
	protected Void call() throws Exception {
		return null;
	}

	public Stage getDialogStage() {
		return dialogStage;
	}
}