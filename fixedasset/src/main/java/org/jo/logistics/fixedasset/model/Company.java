package org.jo.logistics.fixedasset.model;



public class Company{
	public Integer codeCompany;
	public String description;
	private byte[] logo;
	
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Company(Integer codeCompany, String description) {
		super();
		this.codeCompany = codeCompany;
		this.description = description;
	}
	
	

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	
	

	

}
