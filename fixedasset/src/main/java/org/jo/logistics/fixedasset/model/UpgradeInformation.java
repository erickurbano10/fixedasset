package org.jo.logistics.fixedasset.model;

import java.time.LocalDate;
import java.util.Date;

public class UpgradeInformation {
	private Integer idUpgrade;
	private Integer idAsset;
	private Integer codeCompany;
	private Integer idEmployee;
	private Integer board;
	private String serialNumber;
	private String model;
	private String description;
	private String userCreated;
	private String userModificated;
	private Date dateCreated;
	private Date dateModificated;
	private LocalDate date;
	private String commentary;
	
	public UpgradeInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public UpgradeInformation(Integer idUpgrade, Integer idAsset, Integer codeCompany, Integer idEmployee,
			String description, String userCreated, String userModificated, Date dateCreated, Date dateModificated,
			LocalDate date, String commentary) {
		super();
		this.idUpgrade = idUpgrade;
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.idEmployee = idEmployee;
		this.description = description;
		this.userCreated = userCreated;
		this.userModificated = userModificated;
		this.dateCreated = dateCreated;
		this.dateModificated = dateModificated;
		this.date = date;
		this.commentary = commentary;
	}




	public Integer getIdUpgrade() {
		return idUpgrade;
	}

	public void setIdUpgrade(Integer idUpgrade) {
		this.idUpgrade = idUpgrade;
	}
	public Integer getIdAsset() {
		return idAsset;
	}
	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}
	public Integer getCodeCompany() {
		return codeCompany;
	}
	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	public Integer getBoard() {
		return board;
	}
	public void setBoard(Integer board) {
		this.board = board;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserCreated() {
		return userCreated;
	}
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	public String getUserModificated() {
		return userModificated;
	}
	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateModificated() {
		return dateModificated;
	}
	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
}
