package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class reportRetunr {

	private Integer idUpgrade;
	private Integer idEmployee;
	private String nameEmployee;
	private String nameAsset;
	private String lastName;
	private String description;
	private String descriptionm;
	private Date date;
	private String nameCompany;
	private String board;
	private String dayDate;
	private String monthDate;
	private String yearDate;
	private String nameArea;

	public reportRetunr() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Integer getIdUpgrade() {
		return idUpgrade;
	}



	public void setIdUpgrade(Integer idUpgrade) {
		this.idUpgrade = idUpgrade;
	}



	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	
	



	public String getNameEmployee() {
		return nameEmployee;
	}



	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}



	public String getNameAsset() {
		return nameAsset;
	}

	public void setNameAsset(String nameAsset) {
		this.nameAsset = nameAsset;
	}

	

	



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}






	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionm() {
		return descriptionm;
	}

	public void setDescriptionm(String descriptionm) {
		this.descriptionm = descriptionm;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	
	public String getMonthDate() {
		return monthDate;
	}



	public void setMonthDate(String monthDate) {
		this.monthDate = monthDate;
	}



	public String getYearDate() {
		return yearDate;
	}



	public void setYearDate(String yearDate) {
		this.yearDate = yearDate;
	}



	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getDayDate() {
		return dayDate;
	}

	public void setDayDate(String dayDate) {
		this.dayDate = dayDate;
	}

	public String getNameArea() {
		return nameArea;
	}

	public void setNameArea(String nameArea) {
		this.nameArea = nameArea;
	}

}
