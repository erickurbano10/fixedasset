package org.jo.logistics.fixedasset.controller.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

public class LoadImagesController implements Initializable {
	
    @FXML
    private Button btnCancel;

    @FXML
    private Button btnAceptar;

    @FXML
    private Button btnImgFront;

    @FXML
    private Button btnImgBack;
    
    public Image imgFront;
    
    public Image imgBack;
    
	public ByteArrayOutputStream bosFront = new ByteArrayOutputStream();
	
	public ByteArrayOutputStream bosBack = new ByteArrayOutputStream();

	public byte [] imageFront;
	
	public byte [] imageBack;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		initBtnAceptar();
		initBtnCancel();
		initBtnImgFront();
		initBtnImgBack();
	}
	
	private void initBtnCancel() {
		// TODO Auto-generated method stub
		btnCancel.setOnAction(Event->{
		    setImageFront(null);
	        setImageBack(null);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.getScene().getWindow().hide();
		});

	}
	
	private void initBtnAceptar() {
		// TODO Auto-generated method stub
		btnAceptar.setOnAction(Event->{
	    	Stage stage = (Stage) btnAceptar.getScene().getWindow();
			stage.getScene().getWindow().hide();
		});

	}
	
	private void initBtnImgFront() {
		// TODO Auto-generated method stub
        FileChooser fileChooser = new FileChooser();

		btnImgFront.setOnAction(Event->{
			Window window = (((Node) Event.getSource()).getScene()).getWindow();
            File selectedFile = fileChooser.showOpenDialog(window);	
            if(selectedFile!=null) {
            	imgFront = new Image(selectedFile.toURI().toString());
            	FileInputStream fis = null;
            	try {
					fis = new FileInputStream(selectedFile);
					
					byte[] buf = new byte[1024];
				        try {
				            for (int readNum; (readNum = fis.read(buf)) != -1;) {
				                //Writes to this byte array output stream
				                bosFront.write(buf, 0, readNum); 
				            }
				        } catch (IOException ex) {
				        }
				 
				        setImageFront(bosFront.toByteArray());
				   	
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            }     
		});

	}
	
	private void initBtnImgBack() {
		// TODO Auto-generated method stub
        FileChooser fileChooser = new FileChooser();

		btnImgBack.setOnAction(Event->{
			Window window = (((Node) Event.getSource()).getScene()).getWindow();
            File selectedFile = fileChooser.showOpenDialog(window);	
            if(selectedFile!=null) {
            	imgBack = new Image(selectedFile.toURI().toString());
            	FileInputStream fis = null;
            	try {
					fis = new FileInputStream(selectedFile);
					
					byte[] buf = new byte[1024];
				        try {
				            for (int readNum; (readNum = fis.read(buf)) != -1;) {
				                //Writes to this byte array output stream
				                bosBack.write(buf, 0, readNum); 
				            }
				        } catch (IOException ex) {
				        }
				 
				        setImageBack(bosBack.toByteArray());
				    
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            }     
		});

	}

	public byte[] getImageFront() {
		return imageFront;
	}

	public void setImageFront(byte[] imageFront) {
		this.imageFront = imageFront;
	}

	public byte[] getImageBack() {
		return imageBack;
	}

	public void setImageBack(byte[] imageBack) {
		this.imageBack = imageBack;
	}
	
	
}
