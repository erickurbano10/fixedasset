package org.jo.logistics.fixedasset.model;

public class Employeein {

	private Integer idEmployee;
	private String nameEmployee;

	public Employeein() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Employeein(Integer idEmployee, String nameEmployee) {
		super();
		this.idEmployee = idEmployee;
		this.nameEmployee = nameEmployee;

	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getNameEmployee() {
		return nameEmployee;
	}

	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}

}
