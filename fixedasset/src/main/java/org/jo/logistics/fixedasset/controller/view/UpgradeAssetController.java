package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.EditingDatePickerCell;
import org.jo.logistics.fixedasset.model.MaintenanceInformation;
import org.jo.logistics.fixedasset.model.UpgradeInformation;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

public class UpgradeAssetController implements Initializable {
	Integer idAsset;
	Integer codeCompany;
	Integer idEmployee;
	Integer board;
	String serialNumber;
	String model;
	String description;
	String status;
	String userCreated;
	String userModificated;
	String commentary;
	LocalDate date;
	Date dateCreated = new Date();
	Date dateModificated = new Date();
	MaintenanceInformation asset = new MaintenanceInformation();
	SearchComponent search = new SearchComponent();

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuUserRegister;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private MenuItem mnuDown;
	
	@FXML
	private Button btnGenerate;

	@FXML
	private Label lblSearch;

	@FXML
	private Label lblEmployee;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private ComboBox<String> cmbSearh;

	@FXML
	private TableView<UpgradeInformation> tblImproven;

	@FXML
	private TableColumn<UpgradeInformation, LocalDate> tbcdate;

	@FXML
	private TableColumn<UpgradeInformation, String> tbcDescription;

	@FXML
	private TableColumn<UpgradeInformation, String> tbcboard;

	@FXML
	private TableColumn<UpgradeInformation, String> tbcCreationR;

	@FXML
	private TableColumn<UpgradeInformation, String> tbcModificationR;

	@FXML
	private TableColumn<UpgradeInformation, String> tbcComent;

	@FXML
	private SearchComponent cmbSearch;

	@FXML
	private SearchComponent cmbemployee;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillCmbPlaca();
		fillCmbEmployee();
		eventkey();
		initadd();
		initdelete();
	}

	public void fillCmbPlaca() {
		ObservableList<ItemInformation> listItems = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAsset();

			for (ItemInformation itemInformation : list) {
				listItems.add(itemInformation);

			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cmbSearch.initCmbSearch(listItems);
		cmbSearch.initBtnClear();
		cmbSearch.fillCmb(listItems);

	}

	public void eventkey() {

		cmbSearch.getMyComboBox().valueProperty().addListener(new ChangeListener<ItemInformation>() {
			@Override
			public void changed(ObservableValue<? extends ItemInformation> observable, ItemInformation oldValue,
					ItemInformation newValue) {
				// TODO Auto-generated method stub
				if (cmbSearch.getMyComboBox().getValue() != null) {
					if (cmbSearch.getMyComboBox().getValue().getItemCode() != null) {
						view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));
					}
				}
			}
		});
	}

	public void fillCmbEmployee() {

		ObservableList<ItemInformation> listEmployee = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAssetEmployee();

			for (ItemInformation itemInformation : list) {
				listEmployee.addAll(itemInformation);

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmbemployee.initCmbSearch(listEmployee);
		cmbemployee.initBtnClear();
		cmbemployee.fillCmb(listEmployee);

	}

	public void view(Integer idAsset) {


		tbcDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
		tbcCreationR.setCellValueFactory(new PropertyValueFactory<>("userCreated"));
		tbcModificationR.setCellValueFactory(new PropertyValueFactory<>("UserModificated"));
		tbcComent.setCellValueFactory(new PropertyValueFactory<>("Commentary"));
		
		tbcdate.setCellValueFactory(new PropertyValueFactory<UpgradeInformation, LocalDate>("date"));
		tbcdate.setCellFactory(
				new Callback<TableColumn<UpgradeInformation, LocalDate>, TableCell<UpgradeInformation, LocalDate>>() {
					@Override
					public TableCell<UpgradeInformation, LocalDate> call(
							TableColumn<UpgradeInformation, LocalDate> p) {
						return new EditingDatePickerCell<UpgradeInformation, LocalDate>();
					
					}
				});
		
		tbcDescription.setCellFactory(TextFieldTableCell.forTableColumn());
		tbcDescription.setOnEditCommit(e -> {
			e.getTableView().getItems().get(e.getTablePosition().getRow()).setDescription(e.getNewValue());
			System.out.println("des: " + e.getNewValue());
		});

		tbcComent.setCellFactory(TextFieldTableCell.forTableColumn());
		tbcComent.setOnEditCommit(e -> {
			e.getTableView().getItems().get(e.getTablePosition().getRow()).setCommentary(e.getNewValue());
			System.out.println("com : " + e.getNewValue());
		});

		ObservableList<UpgradeInformation> listItems = FXCollections.observableArrayList();

		try {

			List<UpgradeInformation> list = FixedAssetDB.viewtableUpgrade(idAsset);

			for (UpgradeInformation assetsInformation : list) {
				listItems.add(assetsInformation);

			}
			if (listItems.equals(list)) {
				tblImproven.setOnKeyPressed((KeyEvent t) -> {
					KeyCode key = t.getCode();
					if (key == KeyCode.TAB) {
						tblImproven.getFocusModel();
						listItems.add(new UpgradeInformation(null,
								Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()), 1,
								Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()), "xxxx ",
								cmbemployee.getMyComboBox().getValue().getDscription(),
								cmbemployee.getMyComboBox().getValue().getDscription(), dateModificated, dateCreated, date,
								"xxx "));
					}
				});
			}


			tblImproven.setItems(listItems);
			tblImproven.refresh();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		tblImproven.setEditable(true);

	}

	public void initadd() {
		btnAdd.setOnAction(event -> {

			ObservableList<UpgradeInformation> listItems = FXCollections.observableArrayList();
			listItems = tblImproven.getItems();
			try {
				FixedAssetDB.addAssetUpgrade(listItems);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));
		});

	}

	public void initdelete() {
		btnDelete.setOnAction(event -> {

			if (tblImproven.getSelectionModel().getSelectedItem() != null) {

				asset.setIdMaintenance(tblImproven.getSelectionModel().getSelectedItem().getIdUpgrade());

				try {
					FixedAssetDB.deleteMaintenance(asset);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));
			}
		});

	}

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
