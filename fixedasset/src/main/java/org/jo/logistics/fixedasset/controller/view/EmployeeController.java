package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.ComboElement;
import org.jo.logistics.fixedasset.model.Employee;
import org.jo.logistics.fixedasset.model.EmployeeInformation;
import org.jo.logistics.fixedasset.view.ui.EditCellSaveChangeLostFocus;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class EmployeeController implements Initializable {

	EmployeeInformation emp = new EmployeeInformation();
	
	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private MenuItem mnuWorkArea;

	@FXML
	private MenuItem mnuEmployee;
	@FXML
	private TableView<EmployeeInformation> tblEmployee;

	@FXML
	private Label lblMessage;

	@FXML
	private AnchorPane ancMessage;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnNew;

	@FXML
	private Button btnDelete;



	@FXML
	private SearchComponent srchArea;

	@FXML
	private TableColumn<EmployeeInformation, String> tbcName;

	@FXML
	private TableColumn<EmployeeInformation, String> tbcLastName;

	@FXML
	private TableColumn<EmployeeInformation, ComboBox<ComboElement>> tbcWorkPosition;

	@FXML
	private Label lblTitle;

	@FXML
	private SwingNode snEmployee;

	private List<Integer> listUpdate = FXCollections.observableArrayList();

	private MessageBarTask messageBarTask;

	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		editcols();
		initSrchArea();
		initBtnNew();
		initBtnSave();
		iniTBtnDelete();

	}

	public void initSrchArea() {

		ObservableList<org.jo.components.model.ItemInformation> listCmb = FXCollections.observableArrayList();
		List<ItemInformation> listArea = FXCollections.observableArrayList();

		try {
			listArea = FixedAssetDB.getWorkAreaCompany(TemporaryVariables.codeCompanyLogin);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (ItemInformation itemInformation : listArea) {
			listCmb.add(itemInformation);
		}

		srchArea.initBtnClear();
		srchArea.initCmbSearch(listCmb);
		srchArea.fillCmb(listCmb);

		srchArea.getMyComboBox().valueProperty().addListener(new ChangeListener<ItemInformation>() {
			@Override
			public void changed(ObservableValue<? extends ItemInformation> observable, ItemInformation oldValue,
					ItemInformation newValue) {
				// TODO Auto-generated method stub
				if (srchArea.getMyComboBox().getValue() != null) {
					if (srchArea.getMyComboBox().getValue().getItemCode() != null) {
						fillTblEmployee(srchArea.getMyComboBox().getValue().getItemCode());
					}
				}
			}
		});
	}

	public void fillTblEmployee(String codeWorkArea) {

		tblEmployee.getItems().clear();

		tbcName.setCellValueFactory(new PropertyValueFactory<EmployeeInformation, String>("name"));
		tbcLastName.setCellValueFactory(new PropertyValueFactory<EmployeeInformation, String>("lastName"));
		tbcWorkPosition.setCellFactory(
				new Callback<TableColumn<EmployeeInformation, ComboBox<ComboElement>>, TableCell<EmployeeInformation, ComboBox<ComboElement>>>() {
					ObservableList<ComboElement> listPositions = FXCollections.observableArrayList();

					@Override
					public TableCell<EmployeeInformation, ComboBox<ComboElement>> call(
							TableColumn<EmployeeInformation, ComboBox<ComboElement>> param) {
						ComboBox<ComboElement> comboBoxWorkPosition = new ComboBox<ComboElement>();

						TableCell<EmployeeInformation, ComboBox<ComboElement>> cell = new TableCell<EmployeeInformation, ComboBox<ComboElement>>() {
							@Override
							protected void updateItem(ComboBox<ComboElement> item, boolean empty) {
								super.updateItem(item, empty);
								if (empty) {
									setGraphic(null);
									setText(null);
								} else {
									EmployeeInformation Information = getTableView().getItems().get(getIndex());

									try {
										listPositions.clear();
										List<ComboElement> list = FixedAssetDB.getWorkPositionCompany(
												TemporaryVariables.codeCompanyLogin, codeWorkArea);
										for (ComboElement comboElement : list) {
											listPositions.add(comboElement);
										}
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									comboBoxWorkPosition.setItems(listPositions);
									comboBoxWorkPosition.setMaxWidth(Double.MAX_VALUE);
									comboBoxWorkPosition.setMaxHeight(Double.MAX_VALUE);

									ComboElement element = new ComboElement(Information.getCodeWorkPosition(),
											Information.getNameWorkPosition());
									comboBoxWorkPosition.setValue(element);
									setGraphic(comboBoxWorkPosition);
									setText(null);

								}

								comboBoxWorkPosition.valueProperty().addListener(new ChangeListener<ComboElement>() {
									@Override
									public void changed(ObservableValue<? extends ComboElement> observable,
											ComboElement oldValue, ComboElement newValue) {
										// TODO Auto-generated method stub

										if (getIndex() >= 0) {
											getTableView().getItems().get(getIndex())
													.setCodeWorkPosition(comboBoxWorkPosition.getValue().getCode());
											if (getTableView().getItems().get(getIndex()).getIdEmployee() != null) {
												if (!listUpdate.contains(
														getTableView().getItems().get(getIndex()).getIdEmployee())) {
													listUpdate.add(
															getTableView().getItems().get(getIndex()).getIdEmployee());
												}
											}
										}
									}
								});

							}
						};

						comboBoxWorkPosition.setConverter(new StringConverter<ComboElement>() {
							@Override
							public String toString(ComboElement object) {
								// TODO Auto-generated method stub
								String result = "";

								if (object != null) {
									if (object != null) {
										result = object.getDescription().toUpperCase();
									}
								}
								return result;
							}

							@Override
							public ComboElement fromString(String string) {
								// TODO Auto-generated method stub
								return null;
							}
						});

						return cell;

					}

				});

		ObservableList<EmployeeInformation> tblItems = new SimpleListProperty<EmployeeInformation>(
				FXCollections.<EmployeeInformation>observableArrayList());

		try {
			List<EmployeeInformation> info = FixedAssetDB.getEmployee(TemporaryVariables.codeCompanyLogin,
					codeWorkArea);

			for (EmployeeInformation employeeInformation : info) {
				tblItems.add(employeeInformation);
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tblEmployee.setItems(tblItems);
		tblEmployee.refresh();

	}

	public void initBtnNew() {
		btnNew.setOnAction(Event -> {
			Date date = new Date();
			EmployeeInformation emp = new EmployeeInformation(null, TemporaryVariables.codeCompanyLogin, "", "", "", "",
					srchArea.getMyComboBox().getValue().getItemCode(),
					srchArea.getMyComboBox().getValue().getDscription(), TemporaryVariables.userNameLogin, date,
					TemporaryVariables.userNameLogin, date);
			tblEmployee.getItems().add(emp);
			tblEmployee.refresh();
			tblEmployee.scrollTo(tblEmployee.getItems().indexOf(emp));
			tblEmployee.getSelectionModel().select(tblEmployee.getItems().indexOf(emp), tbcName);

		});
	}

	public void initBtnSave() {
		btnSave.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					for (EmployeeInformation employeeInformation : tblEmployee.getItems()) {
						Date date = new Date();
						if (employeeInformation.getIdEmployee() == null) {
							Employee empleado = new Employee(employeeInformation.getIdEmployee(),
									employeeInformation.getCodeCompany(), employeeInformation.getName(),
									employeeInformation.getLastName(), employeeInformation.getCodeWorkPosition(),
									employeeInformation.getCodeWorkArea(), employeeInformation.getUserCreated(),
									employeeInformation.getDateCreated(), employeeInformation.getUserModificated(),
									employeeInformation.getDateModificated());
							try {
								if (!FixedAssetDB.insertEmployee(empleado)) {
									throw new Exception();
								}
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							if (listUpdate.contains(employeeInformation.getIdEmployee())) {
								Employee emp = new Employee(employeeInformation.getIdEmployee(),
										employeeInformation.getCodeCompany(), employeeInformation.getName(),
										employeeInformation.getLastName(), employeeInformation.getCodeWorkPosition(),
										employeeInformation.getCodeWorkArea(), employeeInformation.getUserCreated(),
										employeeInformation.getDateCreated(), TemporaryVariables.userNameLogin, date);
								try {
									if (!FixedAssetDB.updateEmployee(emp)) {
										throw new Exception();
									}
								} catch (ClassNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						}
					}
					fillTblEmployee(srchArea.getMyComboBox().getValue().getItemCode());
					listUpdate.clear();

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Empleado creado correctamente", MessageLevel.SUCCESS);

				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al crear empleado", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	private void iniTBtnDelete() {
		// TODO Auto-generated method stub
		btnDelete.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					if (tblEmployee.getSelectionModel() != null) {
						EmployeeInformation emp = new EmployeeInformation();
						emp = tblEmployee.getSelectionModel().getSelectedItem();
						if (emp.getIdEmployee() != null) {
							try {
								if (FixedAssetDB.deleteEmployee(TemporaryVariables.codeCompanyLogin,
										emp.getIdEmployee())) {
									fillTblEmployee(srchArea.getMyComboBox().getValue().getItemCode());
								} else {
									throw new Exception();
								}

							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							tblEmployee.getItems().remove(tblEmployee.getSelectionModel().getSelectedIndex());
						}
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Empleado eliminado correctamente", MessageLevel.SUCCESS);

				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar empleado", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	private void editcols() {
		tbcName.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcName.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {

				if (!e.getTableView().getItems().get(e.getTablePosition().getRow()).getName()
						.equals(e.getNewValue().toUpperCase())) {
					if (!listUpdate
							.contains(e.getTableView().getItems().get(e.getTablePosition().getRow()).getIdEmployee())) {
						listUpdate.add(e.getTableView().getItems().get(e.getTablePosition().getRow()).getIdEmployee());
					}
				}
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setName(e.getNewValue().toUpperCase());
				tblEmployee.refresh();
			}
		});
		tbcLastName.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcLastName.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {

				if (!e.getTableView().getItems().get(e.getTablePosition().getRow()).getLastName()
						.equals(e.getNewValue().toUpperCase())) {
					if (!listUpdate
							.contains(e.getTableView().getItems().get(e.getTablePosition().getRow()).getIdEmployee())) {
						listUpdate.add(e.getTableView().getItems().get(e.getTablePosition().getRow()).getIdEmployee());
					}
				}
				e.getTableView().getItems().get(e.getTablePosition().getRow())
						.setLastName(e.getNewValue().toUpperCase());
				tblEmployee.refresh();
			}
		});
	}

	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessage, lblMessage, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}


	

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
