package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.AssetDownInformation;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AssetDownController implements Initializable {

	Integer idAsset;
	Integer codeCompany;
	Integer idEmployee;
	Integer board;
	String serialNumber;
	String model;
	String description;
	String status;
	String userCreated;
	String userModificated;
	String commentary;
	Date date = new Date();
	Date dateCreated = new Date();
	Date dateModificated = new Date();
	AssetDownInformation asset = new AssetDownInformation();
	SearchComponent search = new SearchComponent();

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuUserRegister;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private Label lblDescription;

	@FXML
	private TextField txtDescription;

	@FXML
	private TextField txtSerie;

	@FXML
	private Label lblSerieS;

	@FXML
	private TextField txtNumberB;

	@FXML
	private Label lblNumberB;

	@FXML
	private Button btnAdd;

	@FXML
	private TextField txtDown;

	@FXML
	private Label lblDown;

	@FXML
	private Button btnDelete;

	@FXML
	private TableView<AssetDownInformation> tblAssetDown;

	@FXML
	private TableColumn<AssetDownInformation, Integer> tbcidAsset;

	@FXML
	private TableColumn<AssetDownInformation, String> tbcBoard;

	@FXML
	private TableColumn<AssetDownInformation, String> tbcDescription;

	@FXML
	private TableColumn<AssetDownInformation, String> tbcStatus;

	@FXML
	private SearchComponent cmbSearch;

	@FXML
	private SearchComponent cmbemployee;

	@FXML
	private AnchorPane ancMessagge;

	@FXML
	private Label lblMessagge;

	private MessageBarTask messageBarTask;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillCmbPlaca();
		fillCmbEmployee();
		initadd();
		initdelete();
		eventkey();
	}

	public void fillCmbPlaca() {
		ObservableList<ItemInformation> listItems = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAssetDown();

			for (ItemInformation itemInformation : list) {
				listItems.add(itemInformation);

			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cmbSearch.initCmbSearch(listItems);
		cmbSearch.initBtnClear();
		cmbSearch.fillCmb(listItems);

	}

	public void eventkey() {

		cmbSearch.getMyComboBox().valueProperty().addListener(new ChangeListener<ItemInformation>() {
			@Override
			public void changed(ObservableValue<? extends ItemInformation> observable, ItemInformation oldValue,
					ItemInformation newValue) {
				// TODO Auto-generated method stub
				if (cmbSearch.getMyComboBox().getValue() != null) {
					if (cmbSearch.getMyComboBox().getValue().getItemCode() != null) {
						view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));
					}
				}
			}
		});

	}

	public void fillCmbEmployee() {

		ObservableList<ItemInformation> listEmployee = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAssetEmployeeDown();

			for (ItemInformation itemInformation : list) {
				listEmployee.addAll(itemInformation);

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmbemployee.initCmbSearch(listEmployee);
		cmbemployee.initBtnClear();
		cmbemployee.fillCmb(listEmployee);

	}

	public void view(Integer idEmployee) {

		tbcidAsset.setCellValueFactory(new PropertyValueFactory<>("idAsset"));
		tbcBoard.setCellValueFactory(new PropertyValueFactory<>("board"));
		tbcDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
		tbcStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

		ObservableList<AssetDownInformation> listItems = FXCollections.observableArrayList();

		try {
			List<AssetDownInformation> list = FixedAssetDB.viewtableAssetDown(idEmployee);

			for (AssetDownInformation assetsInformation : list) {
				listItems.add(assetsInformation);

			}
			if (listItems.equals(list)) {
				tblAssetDown.setOnKeyPressed((KeyEvent t) -> {
					KeyCode key = t.getCode();
					if (key == KeyCode.TAB) {
						tblAssetDown.getFocusModel();
						listItems.add(new AssetDownInformation(null,
								Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()),
								TemporaryVariables.codeCompanyLogin,
								Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()), "  ",
								cmbemployee.getMyComboBox().getValue().getDscription(),
								cmbemployee.getMyComboBox().getValue().getDscription(), date, dateCreated, date));
					}
				});
			}

			tblAssetDown.setItems(listItems);
			tblAssetDown.refresh();

		} catch (Exception e) {
			e.printStackTrace();
		}
		tblAssetDown.setEditable(true);

	}

	public void initadd() {
		btnAdd.setOnAction(event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					ObservableList<AssetDownInformation> listItems = FXCollections.observableArrayList();
					if (tblAssetDown.getSelectionModel().getSelectedItem() != null) {
						try {
							List<AssetDownInformation> list = new ArrayList<AssetDownInformation>();
							AssetDownInformation name = tblAssetDown.getSelectionModel().getSelectedItem();
							AssetDownInformation namesave = new AssetDownInformation(null,
									Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()),
									TemporaryVariables.codeCompanyLogin,
									Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()), "  ",
									cmbemployee.getMyComboBox().getValue().getDscription(),
									cmbemployee.getMyComboBox().getValue().getDscription(), date, dateCreated, date);
							namesave.setIdAsset(name.getIdAsset());

							list.add(namesave);
							FixedAssetDB.addAssetDown(list);

						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Cargo de trabajo eliminado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar cargo de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});

	}

	public void initdelete() {
		btnDelete.setOnAction(event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					if (tblAssetDown.getSelectionModel().getSelectedItem() != null) {

						asset.setIdDownAsset(tblAssetDown.getSelectionModel().getSelectedItem().getIdDownAsset());

						try {
							FixedAssetDB.deleteDownAsset(asset);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						view(Integer.parseInt(cmbSearch.getMyComboBox().getValue().getItemCode()));
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Cargo de trabajo eliminado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar cargo de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});

	}

	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessagge, lblMessagge, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}

	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
