package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class User {

	private int iduser;
	private String user;
	private String pass;
	private String name;
	private String lastName;
	private String area;
	private String idEmployee;
	private String email;
	private int idRol;
	private String status;
	private int Company;
	private String userCreated;
	private String userModificated;
	private Date dateCreated;
	private Date dateModificated;
	
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getIduser() {
		return iduser;
	}



	public void setIduser(int iduser) {
		this.iduser = iduser;
	}



	public String getUser() {
		return user;
	}



	public void setUser(String user) {
		this.user = user;
	}



	public String getPass() {
		return pass;
	}



	public void setPass(String pass) {
		this.pass = pass;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public String getIdEmployee() {
		return idEmployee;
	}



	public void setIdEmployee(String idEmployee) {
		this.idEmployee = idEmployee;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public int getIdRol() {
		return idRol;
	}



	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public int getCompany() {
		return Company;
	}



	public void setCompany(int company) {
		Company = company;
	}



	public String getUserCreated() {
		return userCreated;
	}



	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}



	public String getUserModificated() {
		return userModificated;
	}



	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}



	public Date getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}



	public Date getDateModificated() {
		return dateModificated;
	}



	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	
	
}
