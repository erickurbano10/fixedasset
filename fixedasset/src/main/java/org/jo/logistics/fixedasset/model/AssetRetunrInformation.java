package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class AssetRetunrInformation {
	private Integer idDevolution;
	private Integer idAsset;
	private Integer codeCompany;
	private Integer idEmployee;
	private String board;
	private String serialNumber;
	private String model;
	private String description;
	private String status;
	private String userCreated;
	private String userModificated;
	private Date dateCreated;
	private Date dateModificated;
	private Date date;
	private String commentary;

	public AssetRetunrInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AssetRetunrInformation(Integer idDevolution, Integer idAsset, Integer codeCompany, Integer idEmployee,
			String description,String userCreated, String userModificated, Date dateCreated,
			Date dateModificated, Date date) {
		super();
		this.idDevolution = idDevolution;
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.idEmployee = idEmployee;
		this.description = description;
		this.userCreated = userCreated;
		this.userModificated = userModificated;
		this.dateCreated = dateCreated;
		this.dateModificated = dateModificated;
		this.date = date;
	}

	public Integer getIdDevolution() {
		return idDevolution;
	}

	public void setIdDevolution(Integer idDevolution) {
		this.idDevolution = idDevolution;
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	
}
