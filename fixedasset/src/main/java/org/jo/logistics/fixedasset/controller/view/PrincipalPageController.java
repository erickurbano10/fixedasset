package org.jo.logistics.fixedasset.controller.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.FixedAsset;
import org.jo.logistics.fixedasset.model.FixedAssetInformation;
import org.jo.logistics.fixedasset.view.ui.EditCellSaveChangeLostFocus;
import org.jo.logistics.fixedasset.view.ui.MagnifierPane;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;

public class PrincipalPageController implements Initializable {

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private MenuItem mnuReturn;	

	@FXML
	private MenuItem mnuWorkArea;

	@FXML
	private MenuItem mnuEmployee;
	
	@FXML
	private MenuItem mnuReportMaintenance;
	   
	@FXML
	private MenuItem mnuoptionDelivery;

	@FXML
	private Label lblDescription;

	@FXML
	private TextField txtDescription;

	@FXML
	private TextField txtSerie;

	@FXML
	private Label lblSerieS;

	@FXML
	private TextField txtNumberB;

	@FXML
	private Label lblNumberB;

	@FXML
	private TableColumn<FixedAssetInformation, Button> tbcBtnFile;

	@FXML
	private TableColumn<FixedAssetInformation, String> tbcSerial;

	@FXML
	private TableColumn<FixedAssetInformation, String> tbcStatus;

	@FXML
	private TableColumn<FixedAssetInformation, String> tbcPlaca;

	@FXML
	private TableColumn<FixedAssetInformation, String> tbcDescription;

	@FXML
	private TableColumn<FixedAssetInformation, String> tbcModel;

	@FXML
	private TableColumn<FixedAssetInformation, Image> tbcPictureFront;

	@FXML
	private TableColumn<FixedAssetInformation, Image> tbcPictureBack;

	@FXML
	private TableView<FixedAssetInformation> tblFixedAsset;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnNew;

	@FXML
	private SearchComponent srchEmployee;

	@FXML
	private Button btnDelete;

	@FXML
	private AnchorPane ancMessage;

	@FXML
	private Label lblMessage;

	public byte[] imgF = null;

	public byte[] imgB = null;

	private MessageBarTask messageBarTask;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		editcols();
		initBtnSave();
		initBtnNew();
		initBtnDelete();
		initSrchEmployee();
	}

	public void initBtnSave() {
		btnSave.setOnAction(event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					for (FixedAssetInformation fixed : tblFixedAsset.getItems()) {
						Date date = new Date();
						FixedAsset fix = new FixedAsset(fixed.getIdAsset(), fixed.getCodeCompany(), fixed.getBoard(),
								fixed.getSerial(), fixed.getModel(), fixed.getDescription(), fixed.getStatus(),
								fixed.getIdEmployee(), fixed.getImgFront(), fixed.getImgBack(),
								TemporaryVariables.userNameLogin, date, TemporaryVariables.userNameLogin, date);
						if (fixed.getIdAsset() != null) {
							try {
								FixedAssetDB.updateFixedAsset(fix);
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							try {
								FixedAssetDB.insertFixedAsset(fix);
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					fillTblAsset(Integer.parseInt(srchEmployee.getMyComboBox().getValue().getItemCode()));
					tblFixedAsset.refresh();

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Activo fijo creado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar activo fijo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	public void initBtnNew() {
		// TODO Auto-generated method stub
		btnNew.setOnAction(event -> {
			Date date = new Date();
			FixedAssetInformation fixed = new FixedAssetInformation(null, TemporaryVariables.codeCompanyLogin, "", "",
					"", "", "LIBRE", Integer.parseInt(srchEmployee.getMyComboBox().getValue().getItemCode()), null,
					null, TemporaryVariables.userNameLogin, date, TemporaryVariables.userNameLogin, date, null, null);
			tblFixedAsset.getItems().add(fixed);
			tblFixedAsset.refresh();
			tblFixedAsset.scrollTo(tblFixedAsset.getItems().indexOf(fixed));
			tblFixedAsset.getSelectionModel().select(tblFixedAsset.getItems().indexOf(fixed), tbcPlaca);
		});
	}

	public void initBtnDelete() {
		// TODO Auto-generated method stub
		btnDelete.setOnAction(event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {

					if (tblFixedAsset.getSelectionModel() != null) {
						FixedAssetInformation fixed = new FixedAssetInformation();
						fixed = tblFixedAsset.getSelectionModel().getSelectedItem();
						if (fixed.getIdEmployee() != null) {
							try {
								if (fixed.getStatus().equals("LIBRE")) {
									if (FixedAssetDB.deleteFixed(TemporaryVariables.codeCompanyLogin,
											fixed.getIdAsset())) {
										fillTblAsset(Integer
												.parseInt(srchEmployee.getMyComboBox().getValue().getItemCode()));
									} else {
										throw new Exception();
									}
								} else {
									throw new Exception();
								}
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							tblFixedAsset.getItems().remove(tblFixedAsset.getSelectionModel().getSelectedIndex());
						}
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Activo fijo eliminado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar activo fijo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	public void initSrchEmployee() {
		ObservableList<ItemInformation> listCmb = FXCollections.observableArrayList();
		List<ItemInformation> listEmployee = FXCollections.observableArrayList();

		try {
			listEmployee = FixedAssetDB.getEmployeeComponent(TemporaryVariables.codeCompanyLogin);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (ItemInformation itemInformation : listEmployee) {
			listCmb.add(itemInformation);
		}

		srchEmployee.initBtnClear();
		srchEmployee.initCmbSearch(listCmb);
		srchEmployee.fillCmb(listCmb);

		srchEmployee.getMyComboBox().valueProperty().addListener(new ChangeListener<ItemInformation>() {
			@Override
			public void changed(ObservableValue<? extends ItemInformation> observable, ItemInformation oldValue,
					ItemInformation newValue) {
				// TODO Auto-generated method stub
				if (srchEmployee.getMyComboBox().getValue() != null) {
					if (srchEmployee.getMyComboBox().getValue().getItemCode() != null) {
						fillTblAsset(Integer.parseInt(srchEmployee.getMyComboBox().getValue().getItemCode()));
					}
				}
			}
		});
	}

	private void fillTblAsset(Integer idEmployee) {
		tblFixedAsset.getItems().clear();

		tbcPlaca.setCellValueFactory(new PropertyValueFactory<FixedAssetInformation, String>("board"));
		tbcSerial.setCellValueFactory(new PropertyValueFactory<FixedAssetInformation, String>("serial"));
		tbcDescription.setCellValueFactory(new PropertyValueFactory<FixedAssetInformation, String>("description"));
		tbcModel.setCellValueFactory(new PropertyValueFactory<FixedAssetInformation, String>("model"));
		tbcStatus.setCellValueFactory(new PropertyValueFactory<FixedAssetInformation, String>("status"));
		tbcPictureFront.setCellValueFactory(new PropertyValueFactory<>("frontPicture"));
		tbcPictureFront.setCellFactory(param -> new ImageTableCell<>());
		tbcPictureBack.setCellValueFactory(new PropertyValueFactory<>("backPicture"));
		tbcPictureBack.setCellFactory(param -> new ImageTableCell<>());

		tbcBtnFile.setCellFactory(
				new Callback<TableColumn<FixedAssetInformation, Button>, TableCell<FixedAssetInformation, Button>>() {
					@Override
					public TableCell<FixedAssetInformation, Button> call(
							TableColumn<FixedAssetInformation, Button> param) {

						Button btnInfo = new Button();
						Image image = new Image(getClass().getResourceAsStream("/images/ImageLoad.png"));
						btnInfo.setGraphic(new ImageView(image));
						btnInfo.setMaxSize(50, 50);
						btnInfo.setFocusTraversable(false);

						TableCell<FixedAssetInformation, Button> cell = new TableCell<FixedAssetInformation, Button>() {
							@Override
							protected void updateItem(Button item, boolean empty) {
								super.updateItem(item, empty);
								if (empty) {
									setGraphic(null);
									setText(null);
								} else {
									btnInfo.setOnAction(event -> {
										Window window = (((Node) event.getSource()).getScene()).getWindow();
										try {

											FXMLLoader loader = new FXMLLoader(
													getClass().getResource("/fxml/LoadImages.fxml"));
											Parent root = (Parent) loader.load();
											Scene scene = new Scene(root);
											Stage stage = new Stage();
											stage.initModality(Modality.APPLICATION_MODAL);
											stage.setScene(scene);
											stage.setOnCloseRequest(e -> {
											});

											stage.showAndWait();

											LoadImagesController controller = loader
													.<LoadImagesController>getController();

											setImgF(controller.imageFront);
											controller.setImageFront(imgF);
											setImgB(controller.imageBack);
											controller.setImageBack(imgB);

											if (imgF != null) {
												getTableView().getItems().get(getIndex()).setImgFront(imgF);
											}

											if (imgB != null) {
												getTableView().getItems().get(getIndex()).setImgBack(imgB);
											}
											tblFixedAsset.refresh();
										} catch (Exception e) {
										}
									});
									setGraphic(btnInfo);
									setText(null);
								}
							}
						};
						return cell;
					}
				});

		ObservableList<FixedAssetInformation> tblfixed = new SimpleListProperty<FixedAssetInformation>(
				FXCollections.<FixedAssetInformation>observableArrayList());

		try {
			List<FixedAssetInformation> info = FixedAssetDB.getFixeds(TemporaryVariables.codeCompanyLogin, idEmployee);
			for (FixedAssetInformation fix : info) {
				if (fix.getImgFront() != null) {
					byte[] img = fix.getImgFront();
					Image image1 = new Image(new ByteArrayInputStream(img));
					ReadOnlyObjectWrapper<Image> front = new ReadOnlyObjectWrapper();
					front.setValue(image1);
					fix.setFrontPicture(front);
				}
				if (fix.getImgBack() != null) {
					byte[] img = fix.getImgBack();
					Image image2 = new Image(new ByteArrayInputStream(img));
					ReadOnlyObjectWrapper<Image> back = new ReadOnlyObjectWrapper();
					back.setValue(image2);
					fix.setBackPicture(back);
				}
				tblfixed.add(fix);
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tblFixedAsset.setItems(tblfixed);
		tblFixedAsset.refresh();
	}

	private class ImageTableCell<S> extends TableCell<S, Image> {
		final ImageView imageView = new ImageView();
		MagnifierPane mag = new MagnifierPane();

		ImageTableCell() {
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		}

		@Override
		protected void updateItem(Image item, boolean empty) {
			super.updateItem(item, empty);
			if (empty || item == null) {
				imageView.setImage(null);
				setText(null);
				setGraphic(null);
			}
			HBox hbox = new HBox();
			hbox.setPadding(new Insets(1, 1, 1, 1));
			hbox.setSpacing(10);
			hbox.setAlignment(Pos.TOP_CENTER);
			imageView.setImage(item);
			imageView.setFitHeight(50);
			imageView.setFitWidth(75);
			hbox.getChildren().add(imageView);
			mag.getChildren().add(hbox);
			mag.setRadius(50.0);
			setGraphic(mag);
		}
	}

	private void editcols() {
		tbcPlaca.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcPlaca.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setBoard(e.getNewValue().toUpperCase());
				tblFixedAsset.refresh();
			}
		});
		tbcSerial.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcSerial.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setSerial(e.getNewValue().toUpperCase());
				tblFixedAsset.refresh();
			}
		});
		tbcDescription.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcDescription.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				e.getTableView().getItems().get(e.getTablePosition().getRow())
						.setDescription(e.getNewValue().toUpperCase());
				tblFixedAsset.refresh();
			}
		});
		tbcModel.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcModel.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setModel(e.getNewValue().toUpperCase());
				tblFixedAsset.refresh();
			}
		});
	}

	public byte[] getImgF() {
		return imgF;
	}

	public void setImgF(byte[] imgF) {
		this.imgF = imgF;
	}

	public byte[] getImgB() {
		return imgB;
	}

	public void setImgB(byte[] imgB) {
		this.imgB = imgB;
	}

	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessage, lblMessage, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onprueba() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportDelivery.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Entrega");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onOptionDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OptionDelivery.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Opciones de Acta");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
}
