package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class DeliveryDet {
	
	public Integer idDeliveryDet;
	public Integer idAsset;
	public Integer codeCompany;
	public Integer idDelivery;
	public String  userCreated;
	public Date    dateCreated;
	public String  userModificated;
	public Date    dateModificated;
	public Integer cant;
	public Boolean  selection;
	
	public DeliveryDet() {
		super();
		// TODO Auto-generated constructor stub
		
	}

	
	

	public DeliveryDet(Integer idDeliveryDet, Integer idAsset, Integer codeCompany, Integer idDelivery,
			String userCreated, Date dateCreated, String userModificated, Date dateModificated,Boolean selection) {
		super();
		this.idDeliveryDet = idDeliveryDet;
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.idDelivery = idDelivery;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModificated = userModificated;
		this.dateModificated = dateModificated;
		this.selection = selection;
	}




	public Integer getIdDeliveryDet() {
		return idDeliveryDet;
	}

	public void setIdDeliveryDet(Integer idDeliveryDet) {
		this.idDeliveryDet = idDeliveryDet;
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public Integer getIdDelivery() {
		return idDelivery;
	}

	public void setIdDelivery(Integer idDelivery) {
		this.idDelivery = idDelivery;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	public Integer getCant() {
		return cant;
	}

	public void setCant(Integer cant) {
		this.cant = cant;
	}




	public Boolean getSelection() {
		return selection;
	}




	public void setSelection(Boolean selection) {
		this.selection = selection;
	}

	
	
}
