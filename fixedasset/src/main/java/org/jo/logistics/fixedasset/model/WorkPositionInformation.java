package org.jo.logistics.fixedasset.model;

public class WorkPositionInformation {
	
	private String code;
	private Integer codeCompany;
	private String codeWorkArea;
	private String nameWorkArea;
	private String name;
	private String description;
	private Boolean headArea;
	
	
	public WorkPositionInformation() {
		super();
		// TODO Auto-generated constructor stub
	}


	public WorkPositionInformation(String code, Integer codeCompany, String codeWorkArea, String nameWorkArea,
			String name, String description, Boolean headArea) {
		super();
		this.code = code;
		this.codeCompany = codeCompany;
		this.codeWorkArea = codeWorkArea;
		this.nameWorkArea = nameWorkArea;
		this.name = name;
		this.description = description;
		this.headArea = headArea;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public Integer getCodeCompany() {
		return codeCompany;
	}


	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}


	public String getCodeWorkArea() {
		return codeWorkArea;
	}


	public void setCodeWorkArea(String codeWorkArea) {
		this.codeWorkArea = codeWorkArea;
	}


	public String getNameWorkArea() {
		return nameWorkArea;
	}


	public void setNameWorkArea(String nameWorkArea) {
		this.nameWorkArea = nameWorkArea;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getHeadArea() {
		return headArea;
	}


	public void setHeadArea(Boolean headArea) {
		this.headArea = headArea;
	}
	

}
