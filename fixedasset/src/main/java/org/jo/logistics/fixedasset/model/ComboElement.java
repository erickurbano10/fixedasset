package org.jo.logistics.fixedasset.model;

public class ComboElement {
	
	private String code;
	private String description;
	
	public ComboElement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ComboElement(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ComboElement [code=" + code + ", description=" + description + "]";
	}
	
	

}
