package org.jo.logistics.fixedasset.model;

public class Active {
	private String A; // Activo
	private String I; // Inactivo

	public String getA() {
		return A;
	}

	public void setA(String a) {
		A = a;
	}

	public String getI() {
		return I;
	}

	public void setI(String i) {
		I = i;
	}

	public Active(String a, String i) {
		super();
		A = a;
		I = i;
	}

	


}
