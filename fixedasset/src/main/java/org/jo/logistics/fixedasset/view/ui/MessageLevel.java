package org.jo.logistics.fixedasset.view.ui;


/**
 * Se crea enumerador para los niveles de los mensajes
 * 
 * @author Fabian Andres Caciedo Cuellar
 *
 */
public enum MessageLevel {
	SUCCESS, EXCEPTION, WARNING
}