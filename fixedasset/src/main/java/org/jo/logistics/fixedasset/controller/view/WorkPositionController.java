package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.WorkPosition;
import org.jo.logistics.fixedasset.model.WorkPositionInformation;
import org.jo.logistics.fixedasset.view.ui.EditCellSaveChangeLostFocus;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class WorkPositionController implements Initializable {

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private MenuItem mnuWorkArea;

	@FXML
	private MenuItem mnuEmployee;

	@FXML
	private Label lblMessagge;

	@FXML
	private TableView<WorkPositionInformation> tblWorkPosition;

	@FXML
	private TableColumn<WorkPositionInformation, String> tbcCode;

	@FXML
	private TableColumn<WorkPositionInformation, String> tbcName;

	@FXML
	private TableColumn<WorkPositionInformation, String> tbcDescription;

	@FXML
	private TableColumn<WorkPositionInformation, Boolean> tbcHeadArea;

	@FXML
	private AnchorPane ancMessagge;

	@FXML
	private Button btnNew;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnSave;

	@FXML
	private SearchComponent srchWorkArea;
	
	private MessageBarTask messageBarTask;

	private List<String> listUpdate = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		initSrchArea();
		initBtnNew();
		initBtnSave();
		initBtnDelete();
	}

	public void initSrchArea() {

		ObservableList<org.jo.components.model.ItemInformation> listCmb = FXCollections.observableArrayList();
		List<ItemInformation> listArea = FXCollections.observableArrayList();

		try {
			listArea = FixedAssetDB.getWorkAreas(TemporaryVariables.codeCompanyLogin);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (ItemInformation itemInformation : listArea) {
			listCmb.add(itemInformation);
		}

		srchWorkArea.initBtnClear();
		srchWorkArea.initCmbSearch(listCmb);
		srchWorkArea.fillCmb(listCmb);

		srchWorkArea.getMyComboBox().valueProperty().addListener(new ChangeListener<ItemInformation>() {
			@Override
			public void changed(ObservableValue<? extends ItemInformation> observable, ItemInformation oldValue,
					ItemInformation newValue) {
				// TODO Auto-generated method stub
				if (srchWorkArea.getMyComboBox().getValue() != null) {
					if (srchWorkArea.getMyComboBox().getValue().getItemCode() != null) {
						fillTblWorkArea(srchWorkArea.getMyComboBox().getValue().getItemCode());
					}
				}
			}
		});
	}

	private void editcols() {
		tbcCode.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcCode.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				if (!e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode()
						.equals(e.getNewValue().toUpperCase())) {
					if (!listUpdate
							.contains(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode())) {
						listUpdate.add(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode());
					}
				}
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setCode(e.getNewValue().toUpperCase());
				tblWorkPosition.refresh();
			}
		});
		tbcName.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcName.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				if (!e.getTableView().getItems().get(e.getTablePosition().getRow()).getName()
						.equals(e.getNewValue().toUpperCase())) {
					if (!listUpdate
							.contains(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode())) {
						listUpdate.add(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode());
					}
				}
				e.getTableView().getItems().get(e.getTablePosition().getRow()).setName(e.getNewValue().toUpperCase());
				tblWorkPosition.refresh();
			}
		});
		tbcDescription.setCellFactory(column -> EditCellSaveChangeLostFocus.createStringEditCell());
		tbcDescription.setOnEditCommit(e -> {
			if (e.getTablePosition().getRow() >= 0) {
				if (!e.getTableView().getItems().get(e.getTablePosition().getRow()).getDescription()
						.equals(e.getNewValue().toUpperCase())) {
					if (!listUpdate
							.contains(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode())) {
						listUpdate.add(e.getTableView().getItems().get(e.getTablePosition().getRow()).getCode());
					}
				}
				e.getTableView().getItems().get(e.getTablePosition().getRow())
						.setDescription(e.getNewValue().toUpperCase());
				tblWorkPosition.refresh();
			}
		});
	}

	public void initBtnNew() {
		btnNew.setOnAction(Event -> {
			WorkPositionInformation pos = new WorkPositionInformation("", TemporaryVariables.codeCompanyLogin,
					srchWorkArea.getMyComboBox().getValue().getItemCode(), "", "", "", false);
			tblWorkPosition.getItems().add(pos);

			tblWorkPosition.refresh();
			tblWorkPosition.scrollTo(tblWorkPosition.getItems().indexOf(pos));
			tblWorkPosition.getSelectionModel().select(tblWorkPosition.getItems().indexOf(pos), tbcCode);
		});
	}

	public void initBtnSave() {
		btnSave.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					for (WorkPositionInformation workPositionInformation : tblWorkPosition.getItems()) {
						Date date = new Date();

						WorkPosition work = new WorkPosition(workPositionInformation.getCode(),
								TemporaryVariables.codeCompanyLogin,
								srchWorkArea.getMyComboBox().getValue().getItemCode(),
								workPositionInformation.getName(), workPositionInformation.getDescription(),
								TemporaryVariables.userNameLogin, date, TemporaryVariables.userNameLogin, date,
								workPositionInformation.getHeadArea());
						try {
							if (!FixedAssetDB.updateWorkPosition(work)) {
								WorkPosition workNew = new WorkPosition(workPositionInformation.getCode(),
										TemporaryVariables.codeCompanyLogin,
										srchWorkArea.getMyComboBox().getValue().getItemCode(),
										workPositionInformation.getName(), workPositionInformation.getDescription(),
										TemporaryVariables.userNameLogin, date, TemporaryVariables.userNameLogin, date,
										workPositionInformation.getHeadArea());
								try {
									if (!FixedAssetDB.insertWorkPosition(workNew)) {
										throw new Exception();
									}
								} catch (ClassNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					fillTblWorkArea(srchWorkArea.getMyComboBox().getValue().getItemCode());
					listUpdate.clear();

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Cargo de trabajo modificado correctamente", MessageLevel.SUCCESS);

				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al modificar cargo de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	public void initBtnDelete() {
		btnDelete.setOnAction(Event -> {

			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					if (tblWorkPosition.getSelectionModel() != null) {
						WorkPositionInformation emp = new WorkPositionInformation();
						emp = tblWorkPosition.getSelectionModel().getSelectedItem();
						if (emp.getCode() != null) {
							try {
								if (FixedAssetDB.deleteWorkPosition(emp)) {
									fillTblWorkArea(srchWorkArea.getMyComboBox().getValue().getItemCode());
								} else {
									throw new Exception();
								}

							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							tblWorkPosition.getItems().remove(tblWorkPosition.getSelectionModel().getSelectedIndex());
						}
					}

					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Cargo de trabajo eliminado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar cargo de trabajo", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	class BooleanCell extends TableCell<WorkPositionInformation, Boolean> {
		private CheckBox checkBox;

		public BooleanCell() {
			checkBox = new CheckBox();
			checkBox.setDisable(false);
			checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if (isEditing())
						commitEdit(newValue == null ? false : newValue);
					getTableView().getItems().get(getIndex()).setHeadArea(newValue);
				}
			});
			this.setGraphic(checkBox);
			this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			this.setEditable(true);
			this.setAlignment(Pos.CENTER);
		}

		@Override
		public void startEdit() {
			super.startEdit();
			if (isEmpty()) {
				return;
			}
			checkBox.setDisable(false);
			checkBox.requestFocus();
		}

		@Override
		public void cancelEdit() {
			super.cancelEdit();
			checkBox.setDisable(true);
		}

		@Override
		public void commitEdit(Boolean value) {
			super.commitEdit(value);
			checkBox.setDisable(true);
		}

		@Override
		public void updateItem(Boolean item, boolean empty) {
			super.updateItem(item, empty);
			if (!isEmpty()) {
				checkBox.setSelected(item);
			}
		}
	};

	public void fillTblWorkArea(String code) {

		tblWorkPosition.getItems().clear();
		tbcCode.setCellValueFactory(new PropertyValueFactory<WorkPositionInformation, String>("code"));
		tbcName.setCellValueFactory(new PropertyValueFactory<WorkPositionInformation, String>("name"));
		tbcDescription.setCellValueFactory(new PropertyValueFactory<WorkPositionInformation, String>("description"));
		tbcHeadArea.setCellValueFactory(new PropertyValueFactory<WorkPositionInformation, Boolean>("headArea"));
		tbcHeadArea.setCellFactory(
				new Callback<TableColumn<WorkPositionInformation, Boolean>, TableCell<WorkPositionInformation, Boolean>>() {
					@Override
					public TableCell<WorkPositionInformation, Boolean> call(
							TableColumn<WorkPositionInformation, Boolean> p) {
						return new BooleanCell();
					}
				});
		tbcHeadArea.setCellValueFactory(new PropertyValueFactory<WorkPositionInformation, Boolean>("headArea"));

		tbcHeadArea.setEditable(true);
		editcols();

		ObservableList<WorkPositionInformation> tblItems = new SimpleListProperty<WorkPositionInformation>(
				FXCollections.<WorkPositionInformation>observableArrayList());

		try {
			List<WorkPositionInformation> info = FixedAssetDB.getWorkPositions(TemporaryVariables.codeCompanyLogin,
					code);

			for (WorkPositionInformation workPositionInformation : info) {
				tblItems.add(workPositionInformation);
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tblWorkPosition.setItems(tblItems);
		tblWorkPosition.refresh();
	}

	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessagge, lblMessagge, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
