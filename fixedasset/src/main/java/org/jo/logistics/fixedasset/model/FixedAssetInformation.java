package org.jo.logistics.fixedasset.model;

import java.util.Date;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.scene.image.Image;

public class FixedAssetInformation {
	
	private Integer idAsset;
	private Integer codeCompany;
	private String board;
	private String serial;
	private String model;
	private String description;
	private String status;
	private Integer idEmployee;
	private ReadOnlyObjectWrapper<Image> frontPicture; 
	private ReadOnlyObjectWrapper<Image> backPicture;
	private String userCreated;
	private Date dateCreated;
	private String userModified;
	private Date dateModified;
	private byte[] imgFront;
	private byte[] imgBack;
	
	public FixedAssetInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FixedAssetInformation(Integer idAsset, Integer codeCompany, String board, String serial, String model,
			String description, String status, Integer idEmployee, Image frontPicture,
			Image backPicture, String userCreated, Date dateCreated, String userModified,
			Date dateModified, byte[] imgFront, byte[] imgBack) {
		super();
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.board = board;
		this.serial = serial;
		this.model = model;
		this.description = description;
		this.status = status;
		this.idEmployee = idEmployee;
		this.frontPicture = new ReadOnlyObjectWrapper<>(frontPicture);
		this.backPicture = new ReadOnlyObjectWrapper<>(backPicture);
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModified = userModified;
		this.dateModified = dateModified;
		this.imgBack = imgBack;
		this.imgFront = imgFront;
		
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Image getFrontPicture() throws NullPointerException{
		if(frontPicture != null) {
			return frontPicture.getValue();

		}else {
			Image image = new Image(getClass().getResourceAsStream("/images/NotFound.png"));
			return  image;
		}
	}
	
	public void setFrontPicture(ReadOnlyObjectWrapper<Image> frontPicture) {
		this.frontPicture = frontPicture;
	}
	
	public Image getBackPicture() throws NullPointerException {
		if(backPicture != null) {
			return backPicture.getValue();

		}else {
			Image image = new Image(getClass().getResourceAsStream("/images/NotFound.png"));
			return image;
		}
	}

	public void setBackPicture(ReadOnlyObjectWrapper<Image> backPicture) {
		this.backPicture = backPicture;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public byte[] getImgFront() {
		return imgFront;
	}

	public void setImgFront(byte[] imgFront) {
		this.imgFront = imgFront;
	}

	public byte[] getImgBack() {
		return imgBack;
	}

	public void setImgBack(byte[] imgBack) {
		this.imgBack = imgBack;
	}
	
	
	
}
