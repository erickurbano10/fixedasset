package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.ComboElement;
import org.jo.logistics.fixedasset.model.DeliveryDet;
import org.jo.logistics.fixedasset.model.DeliveryDetInformation;
import org.jo.logistics.fixedasset.model.DeliveryEnc;
import org.jo.logistics.fixedasset.model.DeliveryEncInformation;
import org.jo.logistics.fixedasset.model.FixedAsset;
import org.jo.logistics.fixedasset.model.FixedAssetInformation;
import org.jo.logistics.fixedasset.model.UserInformation;
import org.jo.logistics.fixedasset.view.ui.MessageBarTask;
import org.jo.logistics.fixedasset.view.ui.MessageLevel;
import org.jo.logistics.fixedasset.view.ui.ProgressIndicatorSceneTask;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;

public class DeliveryCertificateController implements Initializable {

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuUserRegister;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private ComboBox<ComboElement> cmbEmployee;

	@FXML
	private TableColumn<DeliveryDetInformation, Boolean> tbcSelect;

	@FXML
	private HBox btnSave;

	@FXML
	private Label lblarea;

	@FXML
	private Label lblemployee;

	@FXML
	private Label lbldate;

	@FXML
	private Label lblcomments;

	@FXML
	private TextField txtcomments;

	@FXML
	private DatePicker datepicker;

	@FXML
	private AnchorPane tblFixed;

	@FXML
	private Button btnDelete;

	@FXML
	private ComboBox<ComboElement> cmbArea;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcFixed;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcCant;

	@FXML
	private TableColumn<DeliveryEncInformation, LocalDate> tbcDateDelivery;

	@FXML
	private TitledPane tblCertificate;

	@FXML
	private TableColumn<DeliveryEncInformation, String> tbcComments;

	@FXML
	private TableColumn<DeliveryEncInformation, String> tbcHeadArea;

	@FXML
	private TableView<DeliveryDetInformation> tblDeliveryDet;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcBoard;

	@FXML
	private TableColumn<DeliveryDetInformation, String> tbcModel;

	@FXML
	private Button btnNew;
	

	@FXML
	private AnchorPane ancMessage;

	@FXML
	private Label lblMessage;
	
	private MessageBarTask messageBarTask;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillCmbArea();
		initCmbEmployee();
		columnsSize();
		cmbEmployee.setDisable(true);
		initBtnNew();

	}

	private void columnsSize() {
		tbcBoard.prefWidthProperty().bind(tblDeliveryDet.widthProperty().multiply(0.1));
		tbcModel.prefWidthProperty().bind(tblDeliveryDet.widthProperty().multiply(0.1));
		tbcFixed.prefWidthProperty().bind(tblDeliveryDet.widthProperty().multiply(0.6));
		tbcCant.prefWidthProperty().bind(tblDeliveryDet.widthProperty().multiply(0.1));
		tbcSelect.prefWidthProperty().bind(tblDeliveryDet.widthProperty().multiply(0.1));
	}
	

	public void initBtnNew() {
		btnNew.setOnAction(event -> {
			ProgressIndicatorSceneTask progressIndicatorSceneTask = new ProgressIndicatorSceneTask() {

				@Override
				protected Void call() throws Exception {
					Date date = new Date();
					LocalDate localDate = LocalDate.now();
					ZoneId defaultZoneId = ZoneId.systemDefault();

					Date dat = Date.from((datepicker.getValue().atStartOfDay(defaultZoneId).toInstant()));

					datepicker.setValue(LocalDate.now());

					ItemInformation item = new ItemInformation();
					try {
						item = FixedAssetDB.getHeadArea(TemporaryVariables.codeCompanyLogin, cmbArea.getValue().getCode());
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					DeliveryEnc delivery = new DeliveryEnc(null, TemporaryVariables.codeCompanyLogin, txtcomments.getText(),
							TemporaryVariables.userNameLogin, date, TemporaryVariables.userNameLogin, date, dat,
							Integer.parseInt(cmbEmployee.getValue().getCode()), Integer.parseInt(item.getItemCode()));

					try {
						FixedAssetDB.insertDeliveryEnc(delivery);

					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try {

						Integer idDelivery = FixedAssetDB.idDeliveryEnc(Integer.parseInt(cmbEmployee.getValue().getCode()),
								TemporaryVariables.codeCompanyLogin);

						for (DeliveryDetInformation deliveryDetInformation : tblDeliveryDet.getItems()) {
							DeliveryDetInformation deliveryDetInformationn = new DeliveryDetInformation(
									deliveryDetInformation.idDeliveryDet, deliveryDetInformation.getIdAsset(),
									TemporaryVariables.codeCompanyLogin, idDelivery, TemporaryVariables.userNameLogin, dat,
									TemporaryVariables.userNameLogin, date, deliveryDetInformation.getCant(),
									deliveryDetInformation.getSelect());
							try {
								if (deliveryDetInformationn.getSelect()) {
									FixedAssetDB.insertDeliveryDet(deliveryDetInformationn);
									FixedAssetDB.UpdateStatus(deliveryDetInformation.getIdAsset(),
											 TemporaryVariables.codeCompanyLogin,Integer.parseInt(cmbEmployee.getValue().getCode()));

								}
		
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.getMessage();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
					clear();
					
					return null;
				}

				@Override
				protected void succeeded() {
					super.succeeded();
					this.getDialogStage().hide();
					clear();
					clearMessageBar();
					printMessageBar("Acta de Entrega  creado correctamente", MessageLevel.SUCCESS);
				}

				@Override
				protected void failed() {
					super.failed();
					this.getDialogStage().hide();
					clearMessageBar();
					printMessageBar("Error al eliminar Acta de Entrega", MessageLevel.EXCEPTION);
				}

			};

			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(progressIndicatorSceneTask);
			executorService.shutdown();

		});
	}

	private void fillCmbArea() {
		// TODO Auto-generated method stub
		ObservableList<ComboElement> listCmb = FXCollections.observableArrayList();
		List<ComboElement> listArea = FXCollections.observableArrayList();

		cmbArea.valueProperty().addListener(new ChangeListener<ComboElement>() {
			@Override
			public void changed(ObservableValue ov, ComboElement t, ComboElement t1) {
				if (cmbArea.getValue() != null) {
					cmbEmployee.setDisable(false);
					cmbEmployee.getItems().clear();
					tblDeliveryDet.getItems().clear();

					ObservableList<ComboElement> listCmbEmployee = FXCollections.observableArrayList();
					List<ComboElement> listEmployee = FXCollections.observableArrayList();

					try {
						listEmployee = FixedAssetDB.getEmployeeCmb(TemporaryVariables.codeCompanyLogin,
								cmbArea.getValue().getCode());
						for (ComboElement comboElement : listEmployee) {
							listCmbEmployee.add(comboElement);
						}
						cmbEmployee.setItems(listCmbEmployee);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});

		cmbArea.setConverter(new StringConverter<ComboElement>() {
			@Override
			public String toString(ComboElement object) {
				// TODO Auto-generated method stub
				String result = "";

				if (object != null) {
					if (object.getDescription() != null) {
						result = object.getDescription().toUpperCase();
					}
				}
				return result;
			}

			@Override
			public ComboElement fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}

		});

		try {
			listArea = FixedAssetDB.getAreaCmbCompany(TemporaryVariables.codeCompanyLogin);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (ComboElement comboElement : listArea) {
			listCmb.add(comboElement);
		}

		cmbArea.setItems(listCmb);
	}

	public void initCmbEmployee() {
		cmbEmployee.valueProperty().addListener(new ChangeListener<ComboElement>() {
			@Override
			public void changed(ObservableValue ov, ComboElement t, ComboElement t1) {
				if (cmbEmployee.getValue() != null) {
					tblDeliveryDet.getItems().clear();
					fillTblDeliveryDet(Integer.parseInt(cmbEmployee.getValue().getCode()));
				}
			}
		});

		cmbEmployee.setConverter(new StringConverter<ComboElement>() {
			@Override
			public String toString(ComboElement object) {
				// TODO Auto-generated method stub
				String result = "";

				if (object != null) {
					if (object.getDescription() != null) {
						result = object.getDescription().toUpperCase();
					}
				}
				return result;
			}

			@Override
			public ComboElement fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}

		});
	}

	class BooleanCell extends TableCell<DeliveryDetInformation, Boolean> {
		private CheckBox checkBox;

		public BooleanCell() {
			checkBox = new CheckBox();
			checkBox.setDisable(false);
			checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if (isEditing())
						commitEdit(newValue == null ? false : newValue);
					getTableView().getItems().get(getIndex()).setSelect(newValue);
				}
			});
			this.setGraphic(checkBox);
			this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			this.setEditable(true);
			this.setAlignment(Pos.CENTER);
		}

		@Override
		public void startEdit() {
			super.startEdit();
			if (isEmpty()) {
				return;
			}
			checkBox.setDisable(false);
			checkBox.requestFocus();
		}

		@Override
		public void cancelEdit() {
			super.cancelEdit();
			checkBox.setDisable(true);
		}

		@Override
		public void commitEdit(Boolean value) {
			super.commitEdit(value);
			checkBox.setDisable(true);
		}

		@Override
		public void updateItem(Boolean item, boolean empty) {
			super.updateItem(item, empty);
			if (!isEmpty()) {
				checkBox.setSelected(item);
			}
		}
	};

	private void fillTblDeliveryDet(Integer IdEnc) {
		// TODO Auto-generated method stub

		tblDeliveryDet.getItems().clear();
		tbcBoard.setCellValueFactory(new PropertyValueFactory<DeliveryDetInformation, String>("assetBoard"));
		tbcModel.setCellValueFactory(new PropertyValueFactory<DeliveryDetInformation, String>("assetModel"));
		tbcFixed.setCellValueFactory(new PropertyValueFactory<DeliveryDetInformation, String>("assetName"));

		tbcCant.setCellFactory(TextFieldTableCell.forTableColumn());
		tbcCant.setOnEditCommit(e -> {
			e.getTableView().getItems().get(e.getTablePosition().getRow()).setCant(e.getNewValue());
			System.out.println("des: " + e.getNewValue());
		});
		tbcSelect.setCellValueFactory(new PropertyValueFactory<DeliveryDetInformation, Boolean>("select"));
		tbcSelect.setCellFactory(
				new Callback<TableColumn<DeliveryDetInformation, Boolean>, TableCell<DeliveryDetInformation, Boolean>>() {
					@Override
					public TableCell<DeliveryDetInformation, Boolean> call(
							TableColumn<DeliveryDetInformation, Boolean> p) {
						return new BooleanCell();
					}
				});
		tbcSelect.setEditable(true);
		tbcCant.setEditable(true);
		txtcomments.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.TAB) {

					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Confirmacion");
					alert.setHeaderText("Desea seguir ?");
					alert.setContentText("Confirme por Favor");

					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == ButtonType.OK) {

						ObservableList<DeliveryDetInformation> tblItems = new SimpleListProperty<DeliveryDetInformation>(
								FXCollections.<DeliveryDetInformation>observableArrayList());

						try {

							List<DeliveryDetInformation> info = FixedAssetDB
									.getDeliveryDetForIdEnc(TemporaryVariables.codeCompanyLogin);
							for (DeliveryDetInformation deliveryDetInformation : info) {
								tblItems.add(deliveryDetInformation);
							}

						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						tblDeliveryDet.setItems(tblItems);
						tblDeliveryDet.refresh();

					} else {
						txtcomments.setText(null);
						cmbArea.setValue(null);
						cmbEmployee.setValue(null);
						datepicker.setValue(null);
					}
				}

			}
		});

	}

	
	private void clearMessageBar() {
		if (messageBarTask != null && messageBarTask.isRunning()) {
			messageBarTask.cancel();
		}
	}

	private void printMessageBar(String message, MessageLevel msgLevel) {
		clearMessageBar();
		messageBarTask = new MessageBarTask(ancMessage, lblMessage, message, msgLevel, 7000);
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		executorService.execute(messageBarTask);
		executorService.shutdown();
	}


	public void clear() {
		txtcomments.setText("");
		cmbArea.setDisable(false);
		cmbEmployee.setDisable(false);
		datepicker.setDisable(false);
		tblDeliveryDet.getItems().clear();
		tblDeliveryDet.refresh();
	}
	
	public void refresh() {

		tbcBoard.setCellValueFactory(new PropertyValueFactory<>("assetBoard"));
		tbcModel.setCellValueFactory(new PropertyValueFactory<>("assetModel"));
		tbcFixed.setCellValueFactory(new PropertyValueFactory<>("assetName"));
		tbcCant.setCellValueFactory(new PropertyValueFactory<>("cant"));
		tbcSelect.setCellValueFactory(new PropertyValueFactory<>("select"));
		ObservableList<DeliveryDetInformation> listItems = FXCollections.observableArrayList();
		try {
			List<DeliveryDetInformation> list = FixedAssetDB.refreshDelivery();

			for (DeliveryDetInformation delivery : list) {
				listItems.add(delivery);
			}

			tblDeliveryDet.setItems(listItems);
			tblDeliveryDet.refresh();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	

	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}

	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	//donde
	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
