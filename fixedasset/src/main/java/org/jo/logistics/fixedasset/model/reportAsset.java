package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class reportAsset {

	private String nameAsset;
	private String nameCompany;
	private String board;
	private Date date;
	private String dayDate;
	private String monthDate;
	private String yearDate;
	private String nameEmployee;
	private String lastEmployee;

	public reportAsset() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNameAsset() {
		return nameAsset;
	}

	public void setNameAsset(String nameAsset) {
		this.nameAsset = nameAsset;
	}

	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	

	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	public String getDayDate() {
		return dayDate;
	}

	public void setDayDate(String dayDate) {
		this.dayDate = dayDate;
	}

	public String getMonthDate() {
		return monthDate;
	}

	public void setMonthDate(String monthDate) {
		this.monthDate = monthDate;
	}

	public String getYearDate() {
		return yearDate;
	}

	public void setYearDate(String yearDate) {
		this.yearDate = yearDate;
	}

	public String getNameEmployee() {
		return nameEmployee;
	}

	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}

	public String getLastEmployee() {
		return lastEmployee;
	}

	public void setLastEmployee(String lastEmployee) {
		this.lastEmployee = lastEmployee;
	}

}
