package org.jo.logistics.fixedasset.model;

public class ReportDeliveryAssetTwo {
	private String nameEmployee;
	private String lastEmployee;
	private String nameCompany;
	private String companyNit;
	private String nameArea;
	private String dayDate;
	private String monthDate;
	private String yearDate;
	private String board;
	private byte[] photo;
	private byte[] photoBack;
	
	
	
	public ReportDeliveryAssetTwo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getNameEmployee() {
		return nameEmployee;
	}
	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}
	public String getLastEmployee() {
		return lastEmployee;
	}
	public void setLastEmployee(String lastEmployee) {
		this.lastEmployee = lastEmployee;
	}
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	
	public String getCompanyNit() {
		return companyNit;
	}

	public void setCompanyNit(String companyNit) {
		this.companyNit = companyNit;
	}

	public String getNameArea() {
		return nameArea;
	}
	public void setNameArea(String nameArea) {
		this.nameArea = nameArea;
	}
	public String getDayDate() {
		return dayDate;
	}
	public void setDayDate(String dayDate) {
		this.dayDate = dayDate;
	}
	
	public String getMonthDate() {
		return monthDate;
	}

	public void setMonthDate(String monthDate) {
		this.monthDate = monthDate;
	}

	public String getYearDate() {
		return yearDate;
	}

	public void setYearDate(String yearDate) {
		this.yearDate = yearDate;
	}

	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	public byte[] getPhotoBack() {
		return photoBack;
	}
	public void setPhotoBack(byte[] photoBack) {
		this.photoBack = photoBack;
	}
	
	
	
}
