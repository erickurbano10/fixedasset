package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class WorkPosition {
	
	private String code;
	private Integer codeCompany;
	private String codeWorkArea;
	private String name;
	private String description;
	private String userCreated;
	private Date dateCreated;
	private String userModified;
	private Date dateModified;
	private Boolean headArea;
	
	public WorkPosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorkPosition(String code, Integer codeCompany, String codeWorkArea, String name, String description,
			String userCreated, Date dateCreated, String userModified, Date dateModified,Boolean headArea) {
		super();
		this.code = code;
		this.codeCompany = codeCompany;
		this.codeWorkArea = codeWorkArea;
		this.name = name;
		this.description = description;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModified = userModified;
		this.dateModified = dateModified;
		this.headArea = headArea;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getCodeWorkArea() {
		return codeWorkArea;
	}

	public void setCodeWorkArea(String codeWorkArea) {
		this.codeWorkArea = codeWorkArea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	

	public Boolean getHeadArea() {
		return headArea;
	}

	public void setHeadArea(Boolean headArea) {
		this.headArea = headArea;
	}

	@Override
	public String toString() {
		return "WorkPosition [code=" + code + ", codeCompany=" + codeCompany + ", codeWorkArea=" + codeWorkArea
				+ ", name=" + name + ", description=" + description + ", userCreated=" + userCreated + ", dateCreated="
				+ dateCreated + ", userModified=" + userModified + ", dateModified=" + dateModified + "]";
	}

	
}
