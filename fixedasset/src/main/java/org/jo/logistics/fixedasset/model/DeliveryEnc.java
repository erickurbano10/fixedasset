package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class DeliveryEnc {
	
	public Integer idDelivery;
	public Integer codeCompany;
	public String comments;
	public String userCreated;
	public Date dateCreated;
	public String userModificated;
	public Date dateModificated;
	public Date deliveryDate;
	public Integer idEmployee;
	public Integer idHeadArea;
	
	public DeliveryEnc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeliveryEnc(Integer idDelivery, Integer codeCompany, String comments, String userCreated,
			Date dateCreated, String userModificated, Date dateModificated, Date deliveryDate, Integer idEmployee,
			Integer idHeadArea) {
		super();
		this.idDelivery = idDelivery;
		this.codeCompany = codeCompany;
		this.comments = comments;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModificated = userModificated;
		this.dateModificated = dateModificated;
		this.deliveryDate = deliveryDate;
		this.idEmployee = idEmployee;
		this.idHeadArea = idHeadArea;
	}

	public Integer getIdDelivery() {
		return idDelivery;
	}

	public void setIdDelivery(Integer idDelivery) {
		this.idDelivery = idDelivery;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Integer getIdHeadArea() {
		return idHeadArea;
	}

	public void setIdHeadArea(Integer idHeadArea) {
		this.idHeadArea = idHeadArea;
	}
		
}
