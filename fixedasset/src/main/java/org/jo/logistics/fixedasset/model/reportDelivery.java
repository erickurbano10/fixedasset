package org.jo.logistics.fixedasset.model;

import java.util.ArrayList;
import java.util.List;

public class reportDelivery {
	
	
	private String nameEmployee;
	private String lastEmployee;
	private String nameCompany;
	private String companyNit;
	private String nameArea;
	private String dayDate;
	private String monthDate;
	private String yearDate;
	private String description ;
    private String board; 
    private String model;
    private String serie;
    private int cant;
    private String nameBoss;
    private String lastBoss;
    private String areaBoss;
    private byte[] photo;
    private byte [] photoBack;
    
    private List<ReportDeliveryAssetTwo> listSubreport  = new ArrayList<ReportDeliveryAssetTwo>();

	
	public reportDelivery() {
		super();
		// TODO Auto-generated constructor stub
	}



	public List<ReportDeliveryAssetTwo> getListSubreport() {
		return listSubreport;
	}



	public void setListSubreport(List<ReportDeliveryAssetTwo> listSubreport) {
		this.listSubreport = listSubreport;
	}



	public String getNameEmployee() {
		return nameEmployee;
	}

	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}

	public String getLastEmployee() {
		return lastEmployee;
	}

	public void setLastEmployee(String lastEmployee) {
		this.lastEmployee = lastEmployee;
	}

	public String getNameCompany() {
		return nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	

	public String getCompanyNit() {
		return companyNit;
	}



	public void setCompanyNit(String companyNit) {
		this.companyNit = companyNit;
	}



	public String getNameArea() {
		return nameArea;
	}

	public void setNameArea(String nameArea) {
		this.nameArea = nameArea;
	}

	public String getDayDate() {
		return dayDate;
	}

	public void setDayDate(String dayDate) {
		this.dayDate = dayDate;
	}
	
	

	public String getMonthDate() {
		return monthDate;
	}



	public void setMonthDate(String monthDate) {
		this.monthDate = monthDate;
	}



	public String getYearDate() {
		return yearDate;
	}



	public void setYearDate(String yearDate) {
		this.yearDate = yearDate;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

	public String getNameBoss() {
		return nameBoss;
	}

	public void setNameBoss(String nameBoss) {
		this.nameBoss = nameBoss;
	}

	public String getLastBoss() {
		return lastBoss;
	}

	public void setLastBoss(String lastBoss) {
		this.lastBoss = lastBoss;
	}

	public String getAreaBoss() {
		return areaBoss;
	}

	public void setAreaBoss(String areaBoss) {
		this.areaBoss = areaBoss;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] b) {
		this.photo = b;
	}

	public byte[] getPhotoBack() {
		return photoBack;
	}

	public void setPhotoBack(byte[] photoBack) {
		this.photoBack = photoBack;
	}

	

	

	
	
	

}
