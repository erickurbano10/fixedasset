package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class DeliveryDetInformation {

	public Integer idDeliveryDet;
	public Integer idAsset;
	public Integer idEmployee;
	public String assetName;
	public String assetBoard;
	public String assetModel;
	public Integer codeCompany;
	public Integer idDelivery;
	public String userCreated;
	public Date dateCreated;
	public String userModificated;
	public Date dateModificated;
	public String cant;
	public Boolean select;
	public String status;

	public DeliveryDetInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeliveryDetInformation(Integer idDeliveryDet, Integer idAsset, Integer codeCompany, Integer idDelivery,
			String userCreated, Date dateCreated, String userModificated, Date dateModificated,String cant,Boolean select) {
		super();
		this.idDeliveryDet = idDeliveryDet;
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.idDelivery = idDelivery;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModificated = userModificated;
		this.dateModificated = dateModificated;
		this.cant = cant;
		this.select = select;
	}
	public Integer getIdDeliveryDet() {
		return idDeliveryDet;
	}

	public void setIdDeliveryDet(Integer idDeliveryDet) {
		this.idDeliveryDet = idDeliveryDet;
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}
	
	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getAssetBoard() {
		return assetBoard;
	}

	public void setAssetBoard(String assetBoard) {
		this.assetBoard = assetBoard;
	}

	public String getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public Integer getIdDelivery() {
		return idDelivery;
	}

	public void setIdDelivery(Integer idDelivery) {
		this.idDelivery = idDelivery;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}



	public String getCant() {
		return cant;
	}

	public void setCant(String cant) {
		this.cant = cant;
	}

	public Boolean getSelect() {
		return select;
	}

	public void setSelect(Boolean select) {
		this.select = select;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



}
