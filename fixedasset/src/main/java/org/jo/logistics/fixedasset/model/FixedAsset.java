package org.jo.logistics.fixedasset.model;

import java.util.Arrays;
import java.util.Date;

public class FixedAsset {
	
	private Integer idAsset;
	private Integer codeCompany;
	private String board;
	private String serial;
	private String model;
	private String description;
	private String status;
	private Integer idEmployee;
	private byte[] frontPicture; 
	private byte[] backPicture;
	private String userCreated;
	private Date dateCreated;
	private String userModified;
	private Date dateModified;
	
	public FixedAsset() {
		super();
	}

	public FixedAsset(Integer idAsset, Integer codeCompany, String board, String serial, String model,
			String description, String status, Integer idEmployee, byte[] frontPicture, byte[] backPicture,
			String userCreated, Date dateCreated, String userModified, Date dateModified) {
		super();
		this.idAsset = idAsset;
		this.codeCompany = codeCompany;
		this.board = board;
		this.serial = serial;
		this.model = model;
		this.description = description;
		this.status = status;
		this.idEmployee = idEmployee;
		this.frontPicture = frontPicture;
		this.backPicture = backPicture;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModified = userModified;
		this.dateModified = dateModified;
	}

	public Integer getIdAsset() {
		return idAsset;
	}

	public void setIdAsset(Integer idAsset) {
		this.idAsset = idAsset;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public byte[] getFrontPicture() {
		return frontPicture;
	}

	public void setFrontPicture(byte[] frontPicture) {
		this.frontPicture = frontPicture;
	}

	public byte[] getBackPicture() {
		return backPicture;
	}

	public void setBackPicture(byte[] backPicture) {
		this.backPicture = backPicture;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		return "FixedAsset [idAsset=" + idAsset + ", codeCompany=" + codeCompany + ", board=" + board + ", serial="
				+ serial + ", model=" + model + ", description=" + description + ", status=" + status + ", idEmployee="
				+ idEmployee + ", frontPicture=" + Arrays.toString(frontPicture) + ", backPicture="
				+ Arrays.toString(backPicture) + ", userCreated=" + userCreated + ", dateCreated=" + dateCreated
				+ ", userModified=" + userModified + ", dateModified=" + dateModified + "]";
	}
	
}
