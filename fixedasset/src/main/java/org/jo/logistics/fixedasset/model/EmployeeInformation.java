package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class EmployeeInformation {
	
	private Integer idEmployee;
	private Integer codeCompany;
	private String  name;
	private String  lastName;
	private String  codeWorkPosition;
	private String  nameWorkPosition;
	private String  codeWorkArea;
	private String  nameWorkArea;
	private String  userCreated;
	private Date    dateCreated;
	private String 	userModificated;
	private Date 	dateModificated;
	
	public EmployeeInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeInformation(Integer idEmployee, Integer codeCompany, String name, String lastName,
			String codeWorkPosition, String nameWorkPosition, String codeWorkArea, String nameWorkArea,
			String userCreated, Date dateCreated, String userModified, Date dateModified) {
		super();
		this.idEmployee = idEmployee;
		this.codeCompany = codeCompany;
		this.name = name;
		this.lastName = lastName;
		this.codeWorkPosition = codeWorkPosition;
		this.nameWorkPosition = nameWorkPosition;
		this.codeWorkArea = codeWorkArea;
		this.nameWorkArea = nameWorkArea;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModificated = userModified;
		this.dateModificated = dateModified;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCodeWorkPosition() {
		return codeWorkPosition;
	}

	public void setCodeWorkPosition(String codeWorkPosition) {
		this.codeWorkPosition = codeWorkPosition;
	}

	public String getNameWorkPosition() {
		return nameWorkPosition;
	}

	public void setNameWorkPosition(String nameWorkPosition) {
		this.nameWorkPosition = nameWorkPosition;
	}

	public String getCodeWorkArea() {
		return codeWorkArea;
	}

	public void setCodeWorkArea(String codeWorkArea) {
		this.codeWorkArea = codeWorkArea;
	}

	public String getNameWorkArea() {
		return nameWorkArea;
	}

	public void setNameWorkArea(String nameWorkArea) {
		this.nameWorkArea = nameWorkArea;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModificated() {
		return userModificated;
	}

	public void setUserModificated(String userModificated) {
		this.userModificated = userModificated;
	}

	public Date getDateModificated() {
		return dateModificated;
	}

	public void setDateModificated(Date dateModificated) {
		this.dateModificated = dateModificated;
	}

	
	
	

}
