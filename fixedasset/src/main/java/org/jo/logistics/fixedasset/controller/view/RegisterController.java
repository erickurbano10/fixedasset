package org.jo.logistics.fixedasset.controller.view;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class RegisterController implements Initializable {
	
	@FXML
	private TextField txtUser;
	
	@FXML
	private PasswordField pwdPass;
	
	@FXML
	private TextField txtName;
	
	@FXML
	private TextField txtLastName;
	
	@FXML
	private TextField txtAre;
	
	@FXML
	private TextField txtIdEmployee;
	
	@FXML
	private TextField txtEmail;
	
	@FXML
	private ComboBox<String> cmbRol;
	

	@FXML
	private Button btnAdd;
	
	@FXML 
	private Button btnBack;
	
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		iniBack();
		
	}
	
	public void iniBack() {
		btnBack.setOnAction((event) -> {
			(((Node) event.getSource()).getScene()).getWindow().hide();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainView.fxml"));
			Parent root = null;
			try {
				root = (Parent) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("Registro");
			stage.centerOnScreen();
			stage.setMaximized(true);
			stage.setOnCloseRequest(e -> {
				Platform.exit();
				System.exit(0);

			});
			stage.show();

		});

	}
	
	
	
	
}
