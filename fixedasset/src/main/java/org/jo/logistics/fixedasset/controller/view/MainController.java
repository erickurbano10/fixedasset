package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.*;
import org.jo.logistics.fixedasset.model.Company;
import org.jo.logistics.fixedasset.model.User;

public class MainController implements Initializable {

	@FXML
	private Label lblUser;

	@FXML
	private Label lblPass;

	@FXML
	private Label lblVeryid;

	@FXML
	private Label lblValue;

	@FXML
	private TextField txtUser;

	@FXML
	private PasswordField pwdPass;

	@FXML
	private Button btnLogin;

	@FXML
	private Button btnRegister;

	@FXML
	private ComboBox<Company> cmbSelect;

	private int opcion = 1;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initBtnlogin();
		inicmbSelect();
		iniRegister();

	}

	public void initBtnlogin() {
		btnLogin.setOnAction((event) -> {
			if (txtUser.getText().toString() != null && pwdPass.getText().toString() != null
					&& !txtUser.getText().toString().trim().equals("")
					&& !pwdPass.getText().toString().trim().equals("") &&  cmbSelect.getValue().getCodeCompany() != null) {

				User emp = new User();

				emp.setUser(txtUser.getText().toString());
				emp.setPass(pwdPass.getText().toString());

				try {
					if (FixedAssetDB.login(emp)) {

						TemporaryVariables.userNameLogin = txtUser.getText();
						TemporaryVariables.codeCompanyLogin = cmbSelect.getValue().getCodeCompany();

						(((Node) event.getSource()).getScene()).getWindow().hide();

						FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
						Parent root = null;
						try {
							root = (Parent) loader.load();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Stage stage = new Stage();
						Scene scene = new Scene(root);
						stage.setScene(scene);
						stage.setTitle("Pagina Principal");
						stage.centerOnScreen();
						stage.setMaximized(true);
						stage.setOnCloseRequest(e -> {
							Platform.exit();
							System.exit(0);

						});
						stage.show();
					} else {
						lblVeryid.setText("Usuario o Contraseña incorrectos");
					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				lblVeryid.setText("Hay Campos sin completar");
			}

		});

	}

	public void iniRegister() {
		btnRegister.setOnAction((event) -> {
			(((Node) event.getSource()).getScene()).getWindow().hide();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Register.fxml"));
			Parent root = null;
			try {
				root = (Parent) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("Registro");
			stage.centerOnScreen();
			stage.setMaximized(true);
			stage.setOnCloseRequest(e -> {
				Platform.exit();
				System.exit(0);

			});
			stage.show();

		});

	}

	public void inicmbSelect() {

		ObservableList<Company> listitem = FXCollections.observableArrayList();
		List<Company> list = FXCollections.observableArrayList();
		try {
			list = FixedAssetDB.codeCompany();

			for (Company company : list) {
				listitem.add(company);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		combobox();
		cmbSelect.setItems(listitem);

	}

	public void combobox() {

		cmbSelect.setConverter(new StringConverter<Company>() {
			@Override
			public String toString(Company object) {
				// TODO Auto-generated method stub
				String result = "";

				if (object != null) {
					if (object.getDescription() != null) {
						result = object.getDescription().toUpperCase();
					}

				}

				return result;
			}

			@Override
			public Company fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}

		});
	}

}
