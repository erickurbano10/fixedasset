package org.jo.logistics.fixedasset.model;

import java.util.Date;

public class Employee {

	private Integer idEmployee;
	private Integer codeCompany;
	private String  name;
	private String  lastName;
	private String  codeWorkPosition;
	private String  codeWorkArea;
	private String  userCreated;
	private Date    dateCreated;
	private String 	userModified;
	private Date 	dateModified;
	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Employee(Integer idEmployee, Integer codeCompany, String name, String lastName, String codeWorkPosition,
			String codeWorkArea, String userCreated, Date dateCreated, String userModified, Date dateModified) {
		super();
		this.idEmployee = idEmployee;
		this.codeCompany = codeCompany;
		this.name = name;
		this.lastName = lastName;
		this.codeWorkPosition = codeWorkPosition;
		this.codeWorkArea = codeWorkArea;
		this.userCreated = userCreated;
		this.dateCreated = dateCreated;
		this.userModified = userModified;
		this.dateModified = dateModified;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Integer getCodeCompany() {
		return codeCompany;
	}

	public void setCodeCompany(Integer codeCompany) {
		this.codeCompany = codeCompany;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCodeWorkPosition() {
		return codeWorkPosition;
	}

	public void setCodeWorkPosition(String codeWorkPosition) {
		this.codeWorkPosition = codeWorkPosition;
	}

	public String getCodeWorkArea() {
		return codeWorkArea;
	}

	public void setCodeWorkArea(String codeWorkArea) {
		this.codeWorkArea = codeWorkArea;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		return "Employee [idEmployee=" + idEmployee + ", codeCompany=" + codeCompany + ", name=" + name + ", lastName="
				+ lastName + ", codeWorkPosition=" + codeWorkPosition + ", codeWorkArea=" + codeWorkArea
				+ ", userCreated=" + userCreated + ", dateCreated=" + dateCreated + ", userModified=" + userModified
				+ ", dateModified=" + dateModified + "]";
	}
	
}
