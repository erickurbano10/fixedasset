package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import org.jo.components.controller.SearchComponent;
import org.jo.components.model.ItemInformation;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;
import org.jo.logistics.fixedasset.model.ReportMaintenance;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

public class reportMaintenanceController implements Initializable {

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuDown;

	@FXML
	private MenuItem mnuReturn;	

	@FXML
	private MenuItem mnuWorkArea;

	@FXML
	private MenuItem mnuEmployee;
	
	@FXML
	private MenuItem mnuReportMaintenance;

	@FXML
	private Label lblreport;

	@FXML
	private Button btnGenerate;

	@FXML
	private SwingNode snmaintenance;

	@FXML
	private SearchComponent cmbemployee;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillCmbEmployee();
		initBtnGenerateReport();
	}

	public void fillCmbEmployee() {

		ObservableList<ItemInformation> listEmployee = FXCollections.observableArrayList();

		try {
			List<ItemInformation> list = FixedAssetDB.viewAssetEmployeeDown();

			for (ItemInformation itemInformation : list) {
				listEmployee.addAll(itemInformation);

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmbemployee.initCmbSearch(listEmployee);
		cmbemployee.initBtnClear();
		cmbemployee.fillCmb(listEmployee);

	}

	public void initBtnGenerateReport() {
		btnGenerate.setOnAction(event -> {

			URL jasperUrl = this.getClass().getResource("/reports/reportMaintenance.jasper");
			Map<String, Object> parameters = new HashMap<String, Object>();
			JasperReport report = null;
			try {
				report = (JasperReport) JRLoader.loadObject(jasperUrl);
			} catch (JRException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			JRBeanCollectionDataSource coleccion;
			
			coleccion = new JRBeanCollectionDataSource(generateCollection());
			JasperPrint Order = null;
			try {
				Order = JasperFillManager.fillReport(report, parameters, coleccion);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			snmaintenance.setContent(new JRViewer(Order));
			snmaintenance.getContent().setVisible(true);
            cmbemployee.getMyComboBox().getValue().getItemCode();
		});
	}

	public Vector<ReportMaintenance> generateCollection() {
		Integer idEmployee = null;
		List<ReportMaintenance> list = FXCollections.observableArrayList();

		try {
			list = FixedAssetDB.ReportMaintenance(Integer.parseInt(cmbemployee.getMyComboBox().getValue().getItemCode()));
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Vector<ReportMaintenance> collection = new Vector<ReportMaintenance>(list);

		return collection;
	}
	
	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
