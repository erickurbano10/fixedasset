package org.jo.logistics.fixedasset.controller.view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;

import java.util.List;
import java.util.ResourceBundle;

import org.jo.logistics.fixedasset.controller.TemporaryVariables;
import org.jo.logistics.fixedasset.controller.db.FixedAssetDB;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import org.jo.logistics.fixedasset.model.UserInformation;

import org.jo.logistics.fixedasset.model.User;

public class UserRegisterController implements Initializable {
	User emp = new User();

	@FXML
	private MenuBar mnuBarPrincipal;

	@FXML
	private MenuItem mnuDeliveryCertificate;

	@FXML
	private MenuItem mnuTransferCertificate;

	@FXML
	private MenuItem mnuUserRegister;

	@FXML
	private MenuItem mnuAssetRegister;

	@FXML
	private MenuItem mnuHistoryCertificate;

	@FXML
	private MenuItem mnuHistoryReport;

	@FXML
	private MenuItem mnuAssetMaintenance;

	@FXML
	private MenuItem mnuImprovementAsset;

	@FXML
	private MenuItem mnuAdminUser;

	@FXML
	private MenuItem mnuReturn;

	@FXML
	private Label lblUser;

	@FXML
	private TextField txtUser;

	@FXML
	private Label lblPass;

	@FXML
	private PasswordField pwdPass;

	@FXML
	private Label lblNameEmployee;

	@FXML
	private TextField txtName;

	@FXML
	private Label lblLastName;

	@FXML
	private TextField txtLastName;

	@FXML
	private Label lblEmail;

	@FXML
	private TextField txtEmail;

	@FXML
	private TextField txtiduser;

	@FXML
	private Label lblstatus;

	@FXML
	private ComboBox<String> cmbSelectStatus;

	@FXML
	private Label lblselectCompany;

	@FXML
	private ComboBox<String> cmbSelectCompany;

	@FXML
	private Label lblSelectRol;

	@FXML
	public ComboBox<String> cmbRol;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnModificated;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnCancel;

	@FXML
	private Label lblresult;

	@FXML
	private TableView<UserInformation> tblUser;

	@FXML
	private TableColumn<UserInformation, String> tbcUser;

	@FXML
	private TableColumn<UserInformation, String> tbcName;

	@FXML
	private TableColumn<UserInformation, String> tbcLastName;

	@FXML
	private TableColumn<UserInformation, String> tbcEmail;

	private int idRol;
	private String opcion;
	int company;
	Date date = new Date();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		initBtnRegister();
		initBtnModificate();
		initBtnDelete();
		initBtnCancel();
		showclik();

		inicmbRol();
		inicmbSelect();
		inicmbCompany();
		refresh();
		txtUser.setEditable(false);
		pwdPass.setEditable(false);
		txtName.setEditable(false);
		txtLastName.setEditable(false);
		txtEmail.setEditable(false);
		tblUser.setDisable(true);
		cmbRol.setDisable(true);
		cmbSelectCompany.setDisable(true);
		cmbSelectStatus.setDisable(true);
		btnCancel.setDisable(true);

	}

	public void inicmbRol() {
		cmbRol.setItems(FXCollections.observableArrayList("Seleccione Rol", "Administrador", "General"));
		cmbRol.getSelectionModel().select(0);
		cmbRol.setOnAction(event -> {
			if (cmbRol.getValue() != null) {
				if (cmbRol.getValue().equals("Administrador")) {
					idRol = 1;
				} else if (cmbRol.getValue().equals("General")) {
					idRol = 2;
				}
			}
		});
	}

	public void inicmbCompany() {

		cmbSelectCompany.setItems(FXCollections.observableArrayList("Seleccione Compañia", "ZF", "JO"));
		cmbSelectCompany.getSelectionModel().select(0);
		cmbSelectCompany.setOnAction(event -> {
			if (cmbSelectCompany.getValue() != null) {

				if (cmbSelectCompany.getValue().equals("ZF")) {
					company = 1;
				} else if (cmbSelectCompany.getValue().equals("JO")) {
					company = 2;
				}
			}
		});
	}

	public void showclik() {
		tblUser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> observableValue, Object oldValue, Object newValue) {
				if (tblUser.getSelectionModel().getSelectedItem() != null) {
					TableViewSelectionModel<UserInformation> selectionModel = tblUser.getSelectionModel();
					ObservableList<?> selectedCells = selectionModel.getSelectedCells();
					TablePosition<Object, ?> tablePosition = (TablePosition<Object, ?>) selectedCells.get(0);
					Object val = tablePosition.getTableColumn().getCellData(newValue);

					UserInformation item = tblUser.getSelectionModel().getSelectedItem();
					txtUser.setText(item.getUser());
					pwdPass.setText(item.getPassword());
					txtName.setText(item.getName());
					txtLastName.setText(item.getLastName());
					txtEmail.setText(item.getEmail());
					String rol = "";
					String company = "";
					String active = "";
					try {
						rol = FixedAssetDB.getNameRol(item.getIdRol());
						company = FixedAssetDB.getCompany(item.getCompany());
						active = FixedAssetDB.getNameStatus(item.getIdUser());
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					cmbRol.setValue(rol);
					cmbSelectCompany.setValue(company);
					cmbSelectStatus.setValue(active);
					try {
						FixedAssetDB.showClick();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}

	public void refresh() {

		tbcUser.setCellValueFactory(new PropertyValueFactory<>("user"));
		tbcName.setCellValueFactory(new PropertyValueFactory<>("name"));
		tbcLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		tbcEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
		ObservableList<UserInformation> listItems = FXCollections.observableArrayList();

		try {
			List<UserInformation> list = FixedAssetDB.refresh();

			for (UserInformation userInformation : list) {
				listItems.add(userInformation);
			}

			tblUser.setItems(listItems);
			tblUser.refresh();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void initBtnRegister() {
		btnAdd.setOnAction(event -> {
			btnDelete.setDisable(true);
			btnModificated.setDisable(true);
			btnCancel.setDisable(false);
			txtUser.setEditable(true);
			pwdPass.setEditable(true);
			txtName.setEditable(true);
			txtLastName.setEditable(true);
			txtEmail.setEditable(true);
			cmbRol.setDisable(false);
			cmbSelectCompany.setDisable(false);
			cmbSelectStatus.setDisable(false);
			String pass = new String(pwdPass.getText());
			new FixedAssetDB();
			String newpass = FixedAssetDB.base64(pwdPass.getText());

			emp.setUser(txtUser.getText());
			emp.setPass(newpass);
			emp.setName(txtName.getText());
			emp.setLastName(txtLastName.getText());
			emp.setEmail(txtEmail.getText());
			emp.setIdRol(idRol);
			emp.setStatus(opcion);
			emp.setCompany(company);
			emp.setUserCreated(TemporaryVariables.userNameLogin);
			emp.setUserModificated(TemporaryVariables.userNameLogin);
			emp.setDateCreated(date);
			emp.setDateModificated(date);

			if (!txtUser.getText().equals("") && !newpass.equals("") && !txtName.getText().equals("")
					&& !txtLastName.getText().equals("")) {

				try {

					if (FixedAssetDB.register(emp)) {
						txtUser.setEditable(false);
						pwdPass.setEditable(false);
						txtName.setEditable(false);
						txtLastName.setEditable(false);
						txtEmail.setEditable(false);
						cmbRol.setDisable(true);
						cmbSelectCompany.setDisable(true);
						cmbSelectStatus.setDisable(true);
						btnDelete.setDisable(false);
						btnModificated.setDisable(false);
						btnCancel.setDisable(false);

					} else {
						txtUser.setEditable(true);
						pwdPass.setEditable(true);
						txtName.setEditable(true);
						txtLastName.setEditable(true);
						txtEmail.setEditable(true);
						cmbRol.setDisable(false);
						cmbSelectCompany.setDisable(false);
						cmbSelectStatus.setDisable(false);

					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				clear();
				refresh();
			}

		});

	}

	public void initBtnModificate() {
		btnModificated.setOnAction(event -> {
			btnAdd.setDisable(true);
			btnDelete.setDisable(true);
			btnCancel.setDisable(false);
			tblUser.setDisable(false);
			txtUser.setEditable(true);
			pwdPass.setEditable(true);
			txtName.setEditable(true);
			txtLastName.setEditable(true);
			txtEmail.setEditable(true);
			cmbRol.setDisable(false);
			cmbSelectCompany.setDisable(false);
			cmbSelectStatus.setDisable(false);

			if (tblUser.getSelectionModel().getSelectedItem() != null) {
				String passUser = "";
				try {
					passUser = FixedAssetDB.validatePass(tblUser.getSelectionModel().getSelectedItem().getIdUser());
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				String pass = pwdPass.getText();
				if (passUser.equals(pass)) {
					pass = passUser;
				} else {
					pass = FixedAssetDB.Encriptar(pass);
				}

				emp.setIduser(tblUser.getSelectionModel().getSelectedItem().getIdUser());
				emp.setUser(txtUser.getText());
				emp.setPass(pass);
				emp.setName(txtName.getText());
				emp.setLastName(txtLastName.getText());
				emp.setEmail(txtEmail.getText());
				emp.setIdRol(idRol);
				emp.setStatus(opcion);
				emp.setCompany(company);
				emp.setUserModificated(TemporaryVariables.userNameLogin);
				emp.setDateModificated(date);
				clear();
				try {
					if (FixedAssetDB.modificated(emp)) {
						cmbRol.getSelectionModel().select(0);
						cmbSelectCompany.getSelectionModel().select(0);
						cmbSelectStatus.getSelectionModel().select(0);
						tblUser.setDisable(true);
						txtUser.setEditable(false);
						pwdPass.setEditable(false);
						txtName.setEditable(false);
						txtLastName.setEditable(false);
						txtEmail.setEditable(false);
						cmbRol.setDisable(true);
						cmbSelectCompany.setDisable(true);
						cmbSelectStatus.setDisable(true);
						btnAdd.setDisable(false);
						btnDelete.setDisable(false);
						btnCancel.setDisable(false);

					}
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

				showclik();
				refresh();
			}
		});

	}

	public void initBtnDelete() {
		btnDelete.setOnAction(event -> {
			tblUser.setDisable(false);
			btnAdd.setDisable(true);
			btnModificated.setDisable(true);
			btnCancel.setDisable(false);
			if (tblUser.getSelectionModel().getSelectedItem() != null) {

				emp.setIduser(tblUser.getSelectionModel().getSelectedItem().getIdUser());
				btnAdd.setDisable(true);
				btnModificated.setDisable(true);
				try {
					if (FixedAssetDB.delete(emp)) {
						btnAdd.setDisable(false);
						btnModificated.setDisable(false);
						btnCancel.setDisable(false);
						tblUser.setDisable(true);
						cmbRol.setDisable(true);
						cmbSelectCompany.setDisable(true);
						cmbSelectStatus.setDisable(true);

						clear();
						refresh();
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
	}

	public void initBtnCancel() {
		btnCancel.setOnAction(event -> {
			clear();
			tblUser.setDisable(true);
			tblUser.getSelectionModel().clearSelection();
			txtUser.setEditable(false);
			pwdPass.setEditable(false);
			txtName.setEditable(false);
			txtLastName.setEditable(false);
			txtEmail.setEditable(false);
			cmbRol.setDisable(true);
			cmbSelectCompany.setDisable(true);
			cmbSelectStatus.setDisable(true);
			btnAdd.setDisable(false);
			btnModificated.setDisable(false);
			btnDelete.setDisable(false);
			btnCancel.setDisable(true);
		});
	}

	public void inicmbSelect() {

		cmbSelectStatus.setItems(FXCollections.observableArrayList("Select Status", "Active", "Inactive"));
		cmbSelectStatus.getSelectionModel().select(0);
		cmbSelectStatus.setOnAction(event -> {
			if (cmbSelectStatus.getValue().equals("Active")) {
				opcion = "A";
			} else if (cmbSelectStatus.getValue().equals("Inactive")) {
				opcion = "I";
			}
		});
	}

	public void clear() {
		txtUser.setText("");
		pwdPass.setText("");
		txtName.setText("");
		txtLastName.setText("");
		txtEmail.setText("");
		cmbRol.setDisable(false);
		cmbSelectCompany.setDisable(false);
		cmbSelectStatus.setDisable(false);
	}


	@FXML
	private void onPrincipal() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PrincipalPage.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Pagina Principal");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDelivery() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DeliveryCertificate.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Entrega de activos ");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("aqui");
		}

	}


	
	@FXML
	private void onTaslate() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TraslateCertificate.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Acta de Traslado");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}


	@FXML
	private void onAssetRg() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetRegister.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onCertificateH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CertificateHistory.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onReportH() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/HistoryReport.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Historial de Actas");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onMaintenance() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mantenimiento de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onImprovement() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetImprovement.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Mejora de Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onAdminU() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AdminUser.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Administrador de Usuario");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onDown() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetDown.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Baja de Activo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onWorkArea() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkArea.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de areas de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	@FXML
	private void onWorkPosition() {
		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WorkPosition.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de cargos de trabajo");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onEmployee() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Employee.fxml"));

			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Creacion de empleados");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	@FXML
	private void onRet() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AssetReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Registro de Devolucion");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotMaintenenace() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportMaintenance.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mantenimiento");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporotUpgrade() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportUpgrade.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Mejoras");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	
	@FXML
	private void onReporretunr() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportReturn.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Devoluciones");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
	@FXML
	private void onReportAsset() {

		Stage stage = (Stage) mnuBarPrincipal.getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reportAsset.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			stage.setHeight(screenSize.getHeight());
			stage.setWidth(screenSize.getWidth());

			stage.setTitle("Reporte De Activos");
			stage.centerOnScreen();
			stage.setScene(scene);
			stage.setOnCloseRequest(e -> {

				Platform.exit();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			// TODO: handle exception
		}

	}
}
