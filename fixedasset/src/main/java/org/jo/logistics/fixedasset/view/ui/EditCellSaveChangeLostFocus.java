package org.jo.logistics.fixedasset.view.ui;

import javafx.event.Event;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;

public class EditCellSaveChangeLostFocus<S, T> extends TableCell<S, T> {

	private final TextField textField = new TextField();

	private final StringConverter<T> converter;

	public EditCellSaveChangeLostFocus(StringConverter<T> converter) {
		this.converter = converter;

		itemProperty().addListener((obx, oldItem, newItem) -> {

			if (newItem == null || newItem.toString().isEmpty()) {
				setText(null);
			} else {
				setText(converter.toString(newItem));
			}
					
		});
		
		textField.setMaxWidth(Double.MAX_VALUE);
		textField.setMaxHeight(Double.MAX_VALUE);
		setGraphic(textField);
		setContentDisplay(ContentDisplay.TEXT_ONLY);
		
		
		textField.setOnAction(evt -> {
			if(textField.getText() != null) {
			commitEdit(this.converter.fromString(textField.getText().trim()));
			}
		});
		textField.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
			if (!isNowFocused) {
				if(textField.getText() != null) {
				commitEdit(this.converter.fromString(textField.getText().trim()));
				}
			}
		});
		textField.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			if (event.getCode() == KeyCode.ESCAPE) {
				textField.setText(converter.toString(getItem()));
				cancelEdit();
				event.consume();
			} else if (event.getCode() == KeyCode.RIGHT) {
				getTableView().getSelectionModel().selectRightCell();
				event.consume();
			} else if (event.getCode() == KeyCode.LEFT) {
				getTableView().getSelectionModel().selectLeftCell();
				event.consume();
			} else if (event.getCode() == KeyCode.UP) {
				getTableView().getSelectionModel().selectAboveCell();
				event.consume();
			} else if (event.getCode() == KeyCode.DOWN) {
				getTableView().getSelectionModel().selectBelowCell();
				event.consume();
			}else if (event.getCode() == KeyCode.TAB) {
				getTableView().getSelectionModel().selectRightCell();
				event.consume();
			}
		});
	}

	public static final StringConverter<String> IDENTITY_CONVERTER = new StringConverter<String>() {

		@Override
		public String toString(String object) {
			return object;
		}

		@Override
		public String fromString(String string) {
			return string;
		}

	};


	public static <S> EditCellSaveChangeLostFocus<S, String> createStringEditCell() {
		return new EditCellSaveChangeLostFocus<S, String>(IDENTITY_CONVERTER);
	}

	@Override
	public void startEdit() {
		super.startEdit();
		textField.setText(converter.toString(getItem()));
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		textField.requestFocus();
	}

	@Override
	public void cancelEdit() {
		super.cancelEdit();
		setContentDisplay(ContentDisplay.TEXT_ONLY);
	}

	@Override
	public void commitEdit(T item) {

		if (isEditing() && !item.equals(getItem())) {
			TableView<S> table = getTableView();
			if (table != null) {
				TableColumn<S, T> column = getTableColumn();
				CellEditEvent<S, T> event = new CellEditEvent<>(table,
						new TablePosition<S, T>(table, getIndex(), column), TableColumn.editCommitEvent(), item);
				
				Event.fireEvent(column, event);
			}
		}else {
			return;
		}
		
		//super.commitEdit(item);
		//setContentDisplay(ContentDisplay.TEXT_ONLY);
	}

}